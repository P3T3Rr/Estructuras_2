
package Frames;

import Metodos.Metodos;
import java.awt.HeadlessException;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Register extends javax.swing.JFrame {

    Login login;
    Metodos met;

    public Register(Login login,  Metodos met) {
        this.login = login;
        this.met = met;
        initComponents();
        this.setTitle("Register");
        this.setResizable(false);
        this.setSize(1158, 700);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/ico.png")).getImage());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Nombre = new javax.swing.JTextField();
        Cedula = new javax.swing.JTextField();
        Contraseña = new javax.swing.JTextField();
        Telefono = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        Register = new javax.swing.JButton();
        ATRAS = new javax.swing.JButton();
        Close = new javax.swing.JButton();
        fondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(null);

        Nombre.setBackground(new java.awt.Color(51, 51, 51));
        Nombre.setFont(new java.awt.Font("Consolas", 0, 24)); // NOI18N
        Nombre.setForeground(new java.awt.Color(204, 204, 204));
        Nombre.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        getContentPane().add(Nombre);
        Nombre.setBounds(180, 40, 161, 40);

        Cedula.setBackground(new java.awt.Color(51, 51, 51));
        Cedula.setFont(new java.awt.Font("Consolas", 0, 24)); // NOI18N
        Cedula.setForeground(new java.awt.Color(204, 204, 204));
        Cedula.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        Cedula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CedulaActionPerformed(evt);
            }
        });
        getContentPane().add(Cedula);
        Cedula.setBounds(180, 100, 161, 40);

        Contraseña.setBackground(new java.awt.Color(51, 51, 51));
        Contraseña.setFont(new java.awt.Font("Consolas", 0, 24)); // NOI18N
        Contraseña.setForeground(new java.awt.Color(204, 204, 204));
        Contraseña.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        getContentPane().add(Contraseña);
        Contraseña.setBounds(180, 160, 161, 40);

        Telefono.setBackground(new java.awt.Color(51, 51, 51));
        Telefono.setFont(new java.awt.Font("Consolas", 0, 24)); // NOI18N
        Telefono.setForeground(new java.awt.Color(204, 204, 204));
        Telefono.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        getContentPane().add(Telefono);
        Telefono.setBounds(180, 220, 161, 40);

        jLabel1.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 48)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Name");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(70, 40, 80, 50);

        jLabel2.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 48)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Id");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(120, 100, 30, 50);

        jLabel3.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 48)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Password");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(20, 150, 130, 60);

        jLabel4.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 48)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Phone");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(60, 210, 90, 50);

        Register.setBackground(new java.awt.Color(180, 27, 19));
        Register.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 48)); // NOI18N
        Register.setForeground(new java.awt.Color(255, 255, 255));
        Register.setText("REGISTER");
        Register.setBorder(null);
        Register.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RegisterActionPerformed(evt);
            }
        });
        getContentPane().add(Register);
        Register.setBounds(190, 280, 150, 70);

        ATRAS.setBackground(new java.awt.Color(180, 27, 19));
        ATRAS.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 48)); // NOI18N
        ATRAS.setForeground(new java.awt.Color(255, 255, 255));
        ATRAS.setText("CANCEL");
        ATRAS.setBorder(null);
        ATRAS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ATRASActionPerformed(evt);
            }
        });
        getContentPane().add(ATRAS);
        ATRAS.setBounds(20, 280, 150, 70);

        Close.setForeground(new java.awt.Color(0, 0, 0));
        Close.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/close_opt.png"))); // NOI18N
        Close.setBorder(null);
        Close.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CloseActionPerformed(evt);
            }
        });
        getContentPane().add(Close);
        Close.setBounds(1080, 10, 70, 70);

        fondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Login.jpg"))); // NOI18N
        getContentPane().add(fondo);
        fondo.setBounds(0, 0, 1160, 700);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void CedulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CedulaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CedulaActionPerformed

    private void RegisterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RegisterActionPerformed
        try{
            if (Nombre.getText().isEmpty() || Cedula.getText().isEmpty() || Contraseña.getText().isEmpty() || Telefono.getText().isEmpty())
                JOptionPane.showMessageDialog(null, "Incomplete space...!!");
            else{
                String nombre = Nombre.getText();
                int cedula = Integer.parseInt(Cedula.getText());
                String contraseña = Contraseña.getText();
                int telefono = Integer.parseInt(Telefono.getText());
                boolean x = met.insertarUsuario(met.raiz, nombre, cedula, telefono, contraseña);
                if (x){
                    JOptionPane.showMessageDialog(null, "Successful Registration...!!");
                    met.writeUsers();
                    this.dispose();
                }
                else{
                    JOptionPane.showMessageDialog(null, "User already exists...!!");
                    Cedula.setText("");
                }
            }
        }
        catch (HeadlessException | NumberFormatException i) {
            JOptionPane.showMessageDialog(null, "The ID must be numeric...!!");
        }
    }//GEN-LAST:event_RegisterActionPerformed

    private void ATRASActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ATRASActionPerformed
        this.dispose();
    }//GEN-LAST:event_ATRASActionPerformed

    private void CloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CloseActionPerformed
        this.dispose();
    }//GEN-LAST:event_CloseActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ATRAS;
    private javax.swing.JTextField Cedula;
    private javax.swing.JButton Close;
    private javax.swing.JTextField Contraseña;
    private javax.swing.JTextField Nombre;
    private javax.swing.JButton Register;
    private javax.swing.JTextField Telefono;
    private javax.swing.JLabel fondo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    // End of variables declaration//GEN-END:variables
}
