package Frames;

import Clases.Bomberos;
import Clases.Casa;
import Clases.Ciudad;
import Clases.CruzRoja;
import Clases.Edificio;
import Clases.Hospital;
import Clases.Incidente;
import Clases.Mall;
import Clases.Policia;
import Metodos.Metodos;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Simulacion extends javax.swing.JFrame {

    Login login;
    Metodos met;
    Ciudad ciudad;
    boolean edificiosDisponibles = false;
    Incidente incidenteActual;

    public Simulacion(Login login, Metodos met, Ciudad ciudad) {
        initComponents();
        this.setTitle("Simulation");
        this.setResizable(false);
        this.setSize(1500, 870);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/ico.png")).getImage());
        this.login = login;
        this.met = met;
        this.ciudad = ciudad;

        NombreCiudad.setText(ciudad.nombre);
        cargarEdificios();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Insidente1 = new javax.swing.JLabel();
        Insidente2 = new javax.swing.JLabel();
        Insidente3 = new javax.swing.JLabel();
        Insidente4 = new javax.swing.JLabel();
        Insidente5 = new javax.swing.JLabel();
        Insidente6 = new javax.swing.JLabel();
        Insidente7 = new javax.swing.JLabel();
        Insidente8 = new javax.swing.JLabel();
        Insidente9 = new javax.swing.JLabel();
        Insidente10 = new javax.swing.JLabel();
        Insidente11 = new javax.swing.JLabel();
        Insidente12 = new javax.swing.JLabel();
        Insidente13 = new javax.swing.JLabel();
        Insidente14 = new javax.swing.JLabel();
        Insidente15 = new javax.swing.JLabel();
        Insidente16 = new javax.swing.JLabel();
        Insidente17 = new javax.swing.JLabel();
        Insidente18 = new javax.swing.JLabel();
        Insidente19 = new javax.swing.JLabel();
        Insidente20 = new javax.swing.JLabel();
        Insidente21 = new javax.swing.JLabel();
        Insidente22 = new javax.swing.JLabel();
        Insidente23 = new javax.swing.JLabel();
        Insidente24 = new javax.swing.JLabel();
        Insidente25 = new javax.swing.JLabel();
        Insidente26 = new javax.swing.JLabel();
        Insidente27 = new javax.swing.JLabel();
        Insidente28 = new javax.swing.JLabel();
        Insidente29 = new javax.swing.JLabel();
        Insidente30 = new javax.swing.JLabel();
        Insidente31 = new javax.swing.JLabel();
        Insidente32 = new javax.swing.JLabel();
        Insidente33 = new javax.swing.JLabel();
        Insidente34 = new javax.swing.JLabel();
        Insidente35 = new javax.swing.JLabel();
        Insidente36 = new javax.swing.JLabel();
        Edificio1 = new javax.swing.JButton();
        Edificio2 = new javax.swing.JButton();
        Edificio3 = new javax.swing.JButton();
        Edificio4 = new javax.swing.JButton();
        Edificio5 = new javax.swing.JButton();
        Edificio6 = new javax.swing.JButton();
        Edificio7 = new javax.swing.JButton();
        Edificio8 = new javax.swing.JButton();
        Edificio9 = new javax.swing.JButton();
        Edificio10 = new javax.swing.JButton();
        Edificio11 = new javax.swing.JButton();
        Edificio12 = new javax.swing.JButton();
        Edificio13 = new javax.swing.JButton();
        Edificio14 = new javax.swing.JButton();
        Edificio15 = new javax.swing.JButton();
        Edificio16 = new javax.swing.JButton();
        Edificio17 = new javax.swing.JButton();
        Edificio18 = new javax.swing.JButton();
        Edificio19 = new javax.swing.JButton();
        Edificio20 = new javax.swing.JButton();
        Edificio21 = new javax.swing.JButton();
        Edificio22 = new javax.swing.JButton();
        Edificio23 = new javax.swing.JButton();
        Edificio24 = new javax.swing.JButton();
        Edificio25 = new javax.swing.JButton();
        Edificio26 = new javax.swing.JButton();
        Edificio27 = new javax.swing.JButton();
        Edificio28 = new javax.swing.JButton();
        Edificio29 = new javax.swing.JButton();
        Edificio30 = new javax.swing.JButton();
        Edificio31 = new javax.swing.JButton();
        Edificio32 = new javax.swing.JButton();
        Edificio33 = new javax.swing.JButton();
        Edificio34 = new javax.swing.JButton();
        Edificio35 = new javax.swing.JButton();
        Edificio36 = new javax.swing.JButton();
        ModificarIncidente = new javax.swing.JButton();
        EliminarIncidente = new javax.swing.JButton();
        NuevoNombreIncidente = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jLabel75 = new javax.swing.JLabel();
        jLabel64 = new javax.swing.JLabel();
        jLabel74 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();
        Atras = new javax.swing.JButton();
        Guardar = new javax.swing.JButton();
        Close = new javax.swing.JButton();
        NombreCiudad = new javax.swing.JLabel();
        Salud = new javax.swing.JButton();
        Crimen = new javax.swing.JButton();
        Fuego = new javax.swing.JButton();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        Nombre1 = new javax.swing.JTextField();
        Nombre2 = new javax.swing.JTextField();
        Nombre3 = new javax.swing.JTextField();
        jLabel51 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        id = new javax.swing.JLabel();
        tipos = new javax.swing.JLabel();
        nombre = new javax.swing.JLabel();
        Vehiculos = new javax.swing.JLabel();
        FondoSImular = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(null);
        getContentPane().add(Insidente1);
        Insidente1.setBounds(120, 120, 80, 50);
        getContentPane().add(Insidente2);
        Insidente2.setBounds(210, 120, 80, 50);
        getContentPane().add(Insidente3);
        Insidente3.setBounds(120, 210, 80, 50);
        getContentPane().add(Insidente4);
        Insidente4.setBounds(210, 210, 80, 50);
        getContentPane().add(Insidente5);
        Insidente5.setBounds(370, 120, 80, 50);
        getContentPane().add(Insidente6);
        Insidente6.setBounds(460, 120, 80, 50);
        getContentPane().add(Insidente7);
        Insidente7.setBounds(370, 210, 80, 50);
        getContentPane().add(Insidente8);
        Insidente8.setBounds(460, 210, 80, 50);
        getContentPane().add(Insidente9);
        Insidente9.setBounds(620, 120, 80, 50);
        getContentPane().add(Insidente10);
        Insidente10.setBounds(710, 120, 80, 50);
        getContentPane().add(Insidente11);
        Insidente11.setBounds(620, 210, 80, 50);
        getContentPane().add(Insidente12);
        Insidente12.setBounds(710, 210, 80, 50);
        getContentPane().add(Insidente13);
        Insidente13.setBounds(120, 370, 80, 50);
        getContentPane().add(Insidente14);
        Insidente14.setBounds(210, 370, 80, 50);
        getContentPane().add(Insidente15);
        Insidente15.setBounds(120, 460, 80, 50);
        getContentPane().add(Insidente16);
        Insidente16.setBounds(210, 460, 80, 50);
        getContentPane().add(Insidente17);
        Insidente17.setBounds(370, 370, 80, 50);
        getContentPane().add(Insidente18);
        Insidente18.setBounds(460, 370, 80, 50);
        getContentPane().add(Insidente19);
        Insidente19.setBounds(370, 460, 80, 50);
        getContentPane().add(Insidente20);
        Insidente20.setBounds(460, 460, 80, 50);
        getContentPane().add(Insidente21);
        Insidente21.setBounds(620, 370, 80, 50);
        getContentPane().add(Insidente22);
        Insidente22.setBounds(710, 370, 80, 50);
        getContentPane().add(Insidente23);
        Insidente23.setBounds(620, 460, 80, 50);
        getContentPane().add(Insidente24);
        Insidente24.setBounds(710, 460, 80, 50);
        getContentPane().add(Insidente25);
        Insidente25.setBounds(120, 620, 80, 50);
        getContentPane().add(Insidente26);
        Insidente26.setBounds(210, 620, 80, 50);
        getContentPane().add(Insidente27);
        Insidente27.setBounds(120, 710, 80, 50);
        getContentPane().add(Insidente28);
        Insidente28.setBounds(210, 710, 80, 50);
        getContentPane().add(Insidente29);
        Insidente29.setBounds(370, 620, 80, 50);
        getContentPane().add(Insidente30);
        Insidente30.setBounds(460, 620, 80, 50);
        getContentPane().add(Insidente31);
        Insidente31.setBounds(370, 710, 80, 50);
        getContentPane().add(Insidente32);
        Insidente32.setBounds(460, 710, 80, 50);
        getContentPane().add(Insidente33);
        Insidente33.setBounds(620, 620, 80, 50);
        getContentPane().add(Insidente34);
        Insidente34.setBounds(710, 620, 80, 50);
        getContentPane().add(Insidente35);
        Insidente35.setBounds(620, 710, 80, 50);
        getContentPane().add(Insidente36);
        Insidente36.setBounds(710, 710, 80, 50);

        Edificio1.setBackground(new java.awt.Color(51, 51, 51));
        Edificio1.setBorder(null);
        Edificio1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio1ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio1);
        Edificio1.setBounds(120, 90, 80, 80);

        Edificio2.setBackground(new java.awt.Color(51, 51, 51));
        Edificio2.setBorder(null);
        Edificio2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio2ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio2);
        Edificio2.setBounds(210, 90, 80, 80);

        Edificio3.setBackground(new java.awt.Color(51, 51, 51));
        Edificio3.setBorder(null);
        Edificio3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio3ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio3);
        Edificio3.setBounds(120, 180, 80, 80);

        Edificio4.setBackground(new java.awt.Color(51, 51, 51));
        Edificio4.setBorder(null);
        Edificio4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio4ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio4);
        Edificio4.setBounds(210, 180, 80, 80);

        Edificio5.setBackground(new java.awt.Color(51, 51, 51));
        Edificio5.setBorder(null);
        Edificio5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio5ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio5);
        Edificio5.setBounds(370, 90, 80, 80);

        Edificio6.setBackground(new java.awt.Color(51, 51, 51));
        Edificio6.setBorder(null);
        Edificio6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio6ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio6);
        Edificio6.setBounds(460, 90, 80, 80);

        Edificio7.setBackground(new java.awt.Color(51, 51, 51));
        Edificio7.setBorder(null);
        Edificio7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio7ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio7);
        Edificio7.setBounds(370, 180, 80, 80);

        Edificio8.setBackground(new java.awt.Color(51, 51, 51));
        Edificio8.setBorder(null);
        Edificio8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio8ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio8);
        Edificio8.setBounds(460, 180, 80, 80);

        Edificio9.setBackground(new java.awt.Color(51, 51, 51));
        Edificio9.setBorder(null);
        Edificio9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio9ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio9);
        Edificio9.setBounds(620, 90, 80, 80);

        Edificio10.setBackground(new java.awt.Color(51, 51, 51));
        Edificio10.setBorder(null);
        Edificio10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio10ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio10);
        Edificio10.setBounds(710, 90, 80, 80);

        Edificio11.setBackground(new java.awt.Color(51, 51, 51));
        Edificio11.setBorder(null);
        Edificio11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio11ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio11);
        Edificio11.setBounds(620, 180, 80, 80);

        Edificio12.setBackground(new java.awt.Color(51, 51, 51));
        Edificio12.setBorder(null);
        Edificio12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio12ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio12);
        Edificio12.setBounds(710, 180, 80, 80);

        Edificio13.setBackground(new java.awt.Color(51, 51, 51));
        Edificio13.setBorder(null);
        Edificio13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio13ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio13);
        Edificio13.setBounds(120, 340, 80, 80);

        Edificio14.setBackground(new java.awt.Color(51, 51, 51));
        Edificio14.setBorder(null);
        Edificio14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio14ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio14);
        Edificio14.setBounds(210, 340, 80, 80);

        Edificio15.setBackground(new java.awt.Color(51, 51, 51));
        Edificio15.setBorder(null);
        Edificio15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio15ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio15);
        Edificio15.setBounds(120, 430, 80, 80);

        Edificio16.setBackground(new java.awt.Color(51, 51, 51));
        Edificio16.setBorder(null);
        Edificio16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio16ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio16);
        Edificio16.setBounds(210, 430, 80, 80);

        Edificio17.setBackground(new java.awt.Color(51, 51, 51));
        Edificio17.setBorder(null);
        Edificio17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio17ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio17);
        Edificio17.setBounds(370, 340, 80, 80);

        Edificio18.setBackground(new java.awt.Color(51, 51, 51));
        Edificio18.setBorder(null);
        Edificio18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio18ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio18);
        Edificio18.setBounds(460, 340, 80, 80);

        Edificio19.setBackground(new java.awt.Color(51, 51, 51));
        Edificio19.setBorder(null);
        Edificio19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio19ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio19);
        Edificio19.setBounds(370, 430, 80, 80);

        Edificio20.setBackground(new java.awt.Color(51, 51, 51));
        Edificio20.setBorder(null);
        Edificio20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio20ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio20);
        Edificio20.setBounds(460, 430, 80, 80);

        Edificio21.setBackground(new java.awt.Color(51, 51, 51));
        Edificio21.setBorder(null);
        Edificio21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio21ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio21);
        Edificio21.setBounds(620, 340, 80, 80);

        Edificio22.setBackground(new java.awt.Color(51, 51, 51));
        Edificio22.setBorder(null);
        Edificio22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio22ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio22);
        Edificio22.setBounds(710, 340, 80, 80);

        Edificio23.setBackground(new java.awt.Color(51, 51, 51));
        Edificio23.setBorder(null);
        Edificio23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio23ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio23);
        Edificio23.setBounds(620, 430, 80, 80);

        Edificio24.setBackground(new java.awt.Color(51, 51, 51));
        Edificio24.setBorder(null);
        Edificio24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio24ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio24);
        Edificio24.setBounds(710, 430, 80, 80);

        Edificio25.setBackground(new java.awt.Color(51, 51, 51));
        Edificio25.setBorder(null);
        Edificio25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio25ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio25);
        Edificio25.setBounds(120, 590, 80, 80);

        Edificio26.setBackground(new java.awt.Color(51, 51, 51));
        Edificio26.setBorder(null);
        Edificio26.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio26ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio26);
        Edificio26.setBounds(210, 590, 80, 80);

        Edificio27.setBackground(new java.awt.Color(51, 51, 51));
        Edificio27.setBorder(null);
        Edificio27.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio27ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio27);
        Edificio27.setBounds(120, 680, 80, 80);

        Edificio28.setBackground(new java.awt.Color(51, 51, 51));
        Edificio28.setBorder(null);
        Edificio28.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio28ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio28);
        Edificio28.setBounds(210, 680, 80, 80);

        Edificio29.setBackground(new java.awt.Color(51, 51, 51));
        Edificio29.setBorder(null);
        Edificio29.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio29ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio29);
        Edificio29.setBounds(370, 590, 80, 80);

        Edificio30.setBackground(new java.awt.Color(51, 51, 51));
        Edificio30.setBorder(null);
        Edificio30.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio30ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio30);
        Edificio30.setBounds(460, 590, 80, 80);

        Edificio31.setBackground(new java.awt.Color(51, 51, 51));
        Edificio31.setBorder(null);
        Edificio31.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio31ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio31);
        Edificio31.setBounds(370, 680, 80, 80);

        Edificio32.setBackground(new java.awt.Color(51, 51, 51));
        Edificio32.setBorder(null);
        Edificio32.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio32ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio32);
        Edificio32.setBounds(460, 680, 80, 80);

        Edificio33.setBackground(new java.awt.Color(51, 51, 51));
        Edificio33.setBorder(null);
        Edificio33.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio33ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio33);
        Edificio33.setBounds(620, 590, 80, 80);

        Edificio34.setBackground(new java.awt.Color(51, 51, 51));
        Edificio34.setBorder(null);
        Edificio34.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio34ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio34);
        Edificio34.setBounds(710, 590, 80, 80);

        Edificio35.setBackground(new java.awt.Color(51, 51, 51));
        Edificio35.setBorder(null);
        Edificio35.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio35ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio35);
        Edificio35.setBounds(620, 680, 80, 80);

        Edificio36.setBackground(new java.awt.Color(51, 51, 51));
        Edificio36.setBorder(null);
        Edificio36.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio36ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio36);
        Edificio36.setBounds(710, 680, 80, 80);

        ModificarIncidente.setBackground(new java.awt.Color(153, 7, 0));
        ModificarIncidente.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 36)); // NOI18N
        ModificarIncidente.setForeground(new java.awt.Color(255, 255, 255));
        ModificarIncidente.setText("MODIFY");
        ModificarIncidente.setBorder(null);
        ModificarIncidente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ModificarIncidenteActionPerformed(evt);
            }
        });
        getContentPane().add(ModificarIncidente);
        ModificarIncidente.setBounds(1180, 760, 150, 40);

        EliminarIncidente.setBackground(new java.awt.Color(153, 7, 0));
        EliminarIncidente.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 36)); // NOI18N
        EliminarIncidente.setForeground(new java.awt.Color(255, 255, 255));
        EliminarIncidente.setText("DELETE");
        EliminarIncidente.setBorder(null);
        EliminarIncidente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EliminarIncidenteActionPerformed(evt);
            }
        });
        getContentPane().add(EliminarIncidente);
        EliminarIncidente.setBounds(1180, 810, 150, 40);

        NuevoNombreIncidente.setFont(new java.awt.Font("Consolas", 0, 24)); // NOI18N
        NuevoNombreIncidente.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        NuevoNombreIncidente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NuevoNombreIncidenteActionPerformed(evt);
            }
        });
        getContentPane().add(NuevoNombreIncidente);
        NuevoNombreIncidente.setBounds(1150, 710, 180, 40);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle1.png"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(100, 30, 210, 70);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle6.png"))); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(60, 70, 70, 210);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle1.png"))); // NOI18N
        getContentPane().add(jLabel3);
        jLabel3.setBounds(350, 30, 210, 70);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle4.png"))); // NOI18N
        getContentPane().add(jLabel4);
        jLabel4.setBounds(280, 70, 100, 210);

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle1.png"))); // NOI18N
        getContentPane().add(jLabel5);
        jLabel5.setBounds(600, 30, 210, 70);

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle5.png"))); // NOI18N
        getContentPane().add(jLabel6);
        jLabel6.setBounds(780, 70, 70, 210);

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle4.png"))); // NOI18N
        getContentPane().add(jLabel7);
        jLabel7.setBounds(530, 70, 100, 210);

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle3.png"))); // NOI18N
        getContentPane().add(jLabel8);
        jLabel8.setBounds(350, 250, 210, 100);

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle3.png"))); // NOI18N
        getContentPane().add(jLabel9);
        jLabel9.setBounds(100, 250, 210, 100);

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle3.png"))); // NOI18N
        getContentPane().add(jLabel10);
        jLabel10.setBounds(600, 250, 210, 100);

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle6.png"))); // NOI18N
        getContentPane().add(jLabel12);
        jLabel12.setBounds(60, 320, 70, 210);

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle4.png"))); // NOI18N
        getContentPane().add(jLabel14);
        jLabel14.setBounds(280, 320, 100, 210);

        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle5.png"))); // NOI18N
        getContentPane().add(jLabel16);
        jLabel16.setBounds(780, 320, 70, 210);

        jLabel17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle4.png"))); // NOI18N
        getContentPane().add(jLabel17);
        jLabel17.setBounds(530, 320, 100, 210);

        jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle3.png"))); // NOI18N
        getContentPane().add(jLabel18);
        jLabel18.setBounds(350, 500, 210, 100);

        jLabel19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle3.png"))); // NOI18N
        getContentPane().add(jLabel19);
        jLabel19.setBounds(100, 500, 210, 100);

        jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle3.png"))); // NOI18N
        getContentPane().add(jLabel20);
        jLabel20.setBounds(600, 500, 210, 100);

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle6.png"))); // NOI18N
        getContentPane().add(jLabel13);
        jLabel13.setBounds(60, 570, 70, 210);

        jLabel21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle4.png"))); // NOI18N
        getContentPane().add(jLabel21);
        jLabel21.setBounds(280, 570, 100, 210);

        jLabel23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle5.png"))); // NOI18N
        getContentPane().add(jLabel23);
        jLabel23.setBounds(780, 570, 70, 210);

        jLabel24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle4.png"))); // NOI18N
        getContentPane().add(jLabel24);
        jLabel24.setBounds(530, 570, 100, 210);

        jLabel25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle2.png"))); // NOI18N
        getContentPane().add(jLabel25);
        jLabel25.setBounds(350, 750, 210, 70);

        jLabel26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle2.png"))); // NOI18N
        getContentPane().add(jLabel26);
        jLabel26.setBounds(100, 750, 210, 70);

        jLabel27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle2.png"))); // NOI18N
        getContentPane().add(jLabel27);
        jLabel27.setBounds(600, 750, 210, 70);

        jLabel11.setBackground(new java.awt.Color(153, 153, 153));
        jLabel11.setForeground(new java.awt.Color(153, 153, 153));
        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel11);
        jLabel11.setBounds(810, 30, 40, 40);

        jLabel15.setBackground(new java.awt.Color(153, 153, 153));
        jLabel15.setForeground(new java.awt.Color(153, 153, 153));
        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel15);
        jLabel15.setBounds(60, 30, 40, 40);

        jLabel22.setBackground(new java.awt.Color(153, 153, 153));
        jLabel22.setForeground(new java.awt.Color(153, 153, 153));
        jLabel22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel22);
        jLabel22.setBounds(310, 30, 40, 40);

        jLabel28.setBackground(new java.awt.Color(153, 153, 153));
        jLabel28.setForeground(new java.awt.Color(153, 153, 153));
        jLabel28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel28);
        jLabel28.setBounds(560, 30, 40, 40);

        jLabel29.setBackground(new java.awt.Color(153, 153, 153));
        jLabel29.setForeground(new java.awt.Color(153, 153, 153));
        jLabel29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel29);
        jLabel29.setBounds(810, 30, 40, 40);

        jLabel30.setBackground(new java.awt.Color(153, 153, 153));
        jLabel30.setForeground(new java.awt.Color(153, 153, 153));
        jLabel30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel30);
        jLabel30.setBounds(560, 30, 40, 40);

        jLabel31.setBackground(new java.awt.Color(153, 153, 153));
        jLabel31.setForeground(new java.awt.Color(153, 153, 153));
        jLabel31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel31);
        jLabel31.setBounds(310, 30, 40, 40);

        jLabel32.setBackground(new java.awt.Color(153, 153, 153));
        jLabel32.setForeground(new java.awt.Color(153, 153, 153));
        jLabel32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel32);
        jLabel32.setBounds(60, 30, 40, 40);

        jLabel33.setBackground(new java.awt.Color(153, 153, 153));
        jLabel33.setForeground(new java.awt.Color(153, 153, 153));
        jLabel33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel33);
        jLabel33.setBounds(810, 280, 40, 40);

        jLabel34.setBackground(new java.awt.Color(153, 153, 153));
        jLabel34.setForeground(new java.awt.Color(153, 153, 153));
        jLabel34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel34);
        jLabel34.setBounds(560, 280, 40, 40);

        jLabel35.setBackground(new java.awt.Color(153, 153, 153));
        jLabel35.setForeground(new java.awt.Color(153, 153, 153));
        jLabel35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel35);
        jLabel35.setBounds(310, 280, 40, 40);

        jLabel36.setBackground(new java.awt.Color(153, 153, 153));
        jLabel36.setForeground(new java.awt.Color(153, 153, 153));
        jLabel36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel36);
        jLabel36.setBounds(60, 280, 40, 40);

        jLabel37.setBackground(new java.awt.Color(153, 153, 153));
        jLabel37.setForeground(new java.awt.Color(153, 153, 153));
        jLabel37.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel37);
        jLabel37.setBounds(810, 530, 40, 40);

        jLabel38.setBackground(new java.awt.Color(153, 153, 153));
        jLabel38.setForeground(new java.awt.Color(153, 153, 153));
        jLabel38.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel38);
        jLabel38.setBounds(560, 530, 40, 40);

        jLabel39.setBackground(new java.awt.Color(153, 153, 153));
        jLabel39.setForeground(new java.awt.Color(153, 153, 153));
        jLabel39.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel39);
        jLabel39.setBounds(310, 530, 40, 40);

        jLabel40.setBackground(new java.awt.Color(153, 153, 153));
        jLabel40.setForeground(new java.awt.Color(153, 153, 153));
        jLabel40.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel40);
        jLabel40.setBounds(60, 530, 40, 40);

        jLabel41.setBackground(new java.awt.Color(153, 153, 153));
        jLabel41.setForeground(new java.awt.Color(153, 153, 153));
        jLabel41.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel41);
        jLabel41.setBounds(810, 780, 40, 40);

        jLabel42.setBackground(new java.awt.Color(153, 153, 153));
        jLabel42.setForeground(new java.awt.Color(153, 153, 153));
        jLabel42.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel42);
        jLabel42.setBounds(560, 780, 40, 40);

        jLabel43.setBackground(new java.awt.Color(153, 153, 153));
        jLabel43.setForeground(new java.awt.Color(153, 153, 153));
        jLabel43.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel43);
        jLabel43.setBounds(310, 780, 40, 40);

        jLabel44.setBackground(new java.awt.Color(153, 153, 153));
        jLabel44.setForeground(new java.awt.Color(153, 153, 153));
        jLabel44.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel44);
        jLabel44.setBounds(60, 780, 40, 40);

        jLabel75.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cesped.jpg"))); // NOI18N
        getContentPane().add(jLabel75);
        jLabel75.setBounds(0, 440, 500, 490);

        jLabel64.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cesped.jpg"))); // NOI18N
        getContentPane().add(jLabel64);
        jLabel64.setBounds(0, 0, 500, 490);

        jLabel74.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cesped.jpg"))); // NOI18N
        getContentPane().add(jLabel74);
        jLabel74.setBounds(430, 430, 500, 490);

        jLabel59.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cesped.jpg"))); // NOI18N
        getContentPane().add(jLabel59);
        jLabel59.setBounds(430, -50, 500, 490);

        Atras.setForeground(new java.awt.Color(0, 0, 0));
        Atras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/close_opt.png"))); // NOI18N
        Atras.setToolTipText("Main");
        Atras.setBorder(null);
        Atras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AtrasActionPerformed(evt);
            }
        });
        getContentPane().add(Atras);
        Atras.setBounds(1250, 30, 70, 70);

        Guardar.setBackground(new java.awt.Color(0, 0, 0));
        Guardar.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 48)); // NOI18N
        Guardar.setForeground(new java.awt.Color(255, 0, 51));
        Guardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/guardar_opt.png"))); // NOI18N
        Guardar.setToolTipText("Save City");
        Guardar.setAlignmentX(0.5F);
        Guardar.setBorder(null);
        Guardar.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        Guardar.setIconTextGap(10);
        Guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GuardarActionPerformed(evt);
            }
        });
        getContentPane().add(Guardar);
        Guardar.setBounds(1330, 30, 70, 70);

        Close.setForeground(new java.awt.Color(0, 0, 0));
        Close.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/close_opt1.png"))); // NOI18N
        Close.setToolTipText("Close");
        Close.setBorder(null);
        Close.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CloseActionPerformed(evt);
            }
        });
        getContentPane().add(Close);
        Close.setBounds(1410, 30, 70, 70);

        NombreCiudad.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 60)); // NOI18N
        NombreCiudad.setForeground(new java.awt.Color(204, 255, 255));
        NombreCiudad.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        NombreCiudad.setText("San Carlos");
        getContentPane().add(NombreCiudad);
        NombreCiudad.setBounds(930, 30, 250, 70);

        Salud.setBackground(new java.awt.Color(153, 7, 0));
        Salud.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 48)); // NOI18N
        Salud.setForeground(new java.awt.Color(255, 255, 255));
        Salud.setText("HEALTH");
        Salud.setBorder(null);
        Salud.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SaludActionPerformed(evt);
            }
        });
        getContentPane().add(Salud);
        Salud.setBounds(1290, 320, 150, 60);

        Crimen.setBackground(new java.awt.Color(153, 7, 0));
        Crimen.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 48)); // NOI18N
        Crimen.setForeground(new java.awt.Color(255, 255, 255));
        Crimen.setText("CRIME");
        Crimen.setBorder(null);
        Crimen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CrimenActionPerformed(evt);
            }
        });
        getContentPane().add(Crimen);
        Crimen.setBounds(1290, 450, 150, 60);

        Fuego.setBackground(new java.awt.Color(153, 7, 0));
        Fuego.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 48)); // NOI18N
        Fuego.setForeground(new java.awt.Color(255, 255, 255));
        Fuego.setText("FIRE");
        Fuego.setBorder(null);
        Fuego.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FuegoActionPerformed(evt);
            }
        });
        getContentPane().add(Fuego);
        Fuego.setBounds(1290, 190, 150, 60);

        jLabel45.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 48)); // NOI18N
        jLabel45.setForeground(new java.awt.Color(255, 255, 255));
        jLabel45.setText("NAME");
        getContentPane().add(jLabel45);
        jLabel45.setBounds(1000, 200, 70, 40);

        jLabel46.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 48)); // NOI18N
        jLabel46.setForeground(new java.awt.Color(255, 255, 255));
        jLabel46.setText("NAME");
        getContentPane().add(jLabel46);
        jLabel46.setBounds(1000, 330, 70, 40);

        jLabel48.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 48)); // NOI18N
        jLabel48.setForeground(new java.awt.Color(255, 255, 255));
        jLabel48.setText("NAME");
        getContentPane().add(jLabel48);
        jLabel48.setBounds(1000, 460, 70, 40);

        jLabel47.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 36)); // NOI18N
        jLabel47.setForeground(new java.awt.Color(255, 255, 255));
        jLabel47.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel47.setText("VEHICLES");
        getContentPane().add(jLabel47);
        jLabel47.setBounds(1340, 650, 100, 30);

        jLabel49.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 36)); // NOI18N
        jLabel49.setForeground(new java.awt.Color(255, 255, 255));
        jLabel49.setText("ID :");
        getContentPane().add(jLabel49);
        jLabel49.setBounds(970, 650, 40, 40);

        jLabel50.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 36)); // NOI18N
        jLabel50.setForeground(new java.awt.Color(255, 255, 255));
        jLabel50.setText("NAME :");
        getContentPane().add(jLabel50);
        jLabel50.setBounds(970, 790, 70, 40);

        jLabel52.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 36)); // NOI18N
        jLabel52.setForeground(new java.awt.Color(255, 255, 255));
        jLabel52.setText("TYPE :");
        jLabel52.setToolTipText("");
        getContentPane().add(jLabel52);
        jLabel52.setBounds(970, 720, 60, 40);

        Nombre1.setEditable(false);
        Nombre1.setFont(new java.awt.Font("Consolas", 0, 24)); // NOI18N
        Nombre1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        getContentPane().add(Nombre1);
        Nombre1.setBounds(1100, 200, 160, 40);

        Nombre2.setEditable(false);
        Nombre2.setFont(new java.awt.Font("Consolas", 0, 24)); // NOI18N
        Nombre2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        Nombre2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Nombre2ActionPerformed(evt);
            }
        });
        getContentPane().add(Nombre2);
        Nombre2.setBounds(1100, 330, 160, 40);

        Nombre3.setEditable(false);
        Nombre3.setFont(new java.awt.Font("Consolas", 0, 24)); // NOI18N
        Nombre3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        getContentPane().add(Nombre3);
        Nombre3.setBounds(1100, 460, 160, 40);

        jLabel51.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 48)); // NOI18N
        jLabel51.setForeground(new java.awt.Color(255, 255, 255));
        jLabel51.setText("INCIDENT'S DATA");
        getContentPane().add(jLabel51);
        jLabel51.setBounds(1010, 580, 200, 40);

        jLabel53.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 36)); // NOI18N
        jLabel53.setForeground(new java.awt.Color(255, 255, 255));
        jLabel53.setText("NUMBER OF ");
        getContentPane().add(jLabel53);
        jLabel53.setBounds(1340, 610, 110, 30);

        id.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 36)); // NOI18N
        id.setForeground(new java.awt.Color(204, 255, 255));
        getContentPane().add(id);
        id.setBounds(1040, 650, 100, 40);

        tipos.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 36)); // NOI18N
        tipos.setForeground(new java.awt.Color(204, 255, 255));
        getContentPane().add(tipos);
        tipos.setBounds(1040, 720, 100, 40);

        nombre.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 36)); // NOI18N
        nombre.setForeground(new java.awt.Color(204, 255, 255));
        getContentPane().add(nombre);
        nombre.setBounds(1040, 790, 100, 40);

        Vehiculos.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 80)); // NOI18N
        Vehiculos.setForeground(new java.awt.Color(204, 255, 255));
        Vehiculos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Vehiculos.setText("0");
        getContentPane().add(Vehiculos);
        Vehiculos.setBounds(1330, 690, 120, 140);

        FondoSImular.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/FondoJuego.jpg"))); // NOI18N
        getContentPane().add(FondoSImular);
        FondoSImular.setBounds(930, 210, 580, 740);

        jLabel55.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/FondoJuego.jpg"))); // NOI18N
        getContentPane().add(jLabel55);
        jLabel55.setBounds(930, 0, 580, 720);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Edificio1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio1ActionPerformed
        if (ciudad.E1.incidente != null) {
            incidenteActual = ciudad.E1.incidente;
        }
        if (ciudad.E1.tipo != null) {
            if (ciudad.E1.incidente != null) {
                id.setText(ciudad.E1.incidente.Id + "");
                tipos.setText(ciudad.E1.incidente.tipo);
                nombre.setText(ciudad.E1.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio1ActionPerformed

    private void Edificio3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio3ActionPerformed
        if (ciudad.E19.incidente != null) {
            incidenteActual = ciudad.E19.incidente;
        }
        if (ciudad.E19.tipo != null) {
            if (ciudad.E19.incidente != null) {
                id.setText(ciudad.E19.incidente.Id + "");
                tipos.setText(ciudad.E19.incidente.tipo);
                nombre.setText(ciudad.E19.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio3ActionPerformed

    private void AtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AtrasActionPerformed
        limpiarSimulaciones();
        Menu frame = new Menu(login, met);
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_AtrasActionPerformed

    private void GuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GuardarActionPerformed
        limpiarSimulaciones();
        met.writeUsers();
    }//GEN-LAST:event_GuardarActionPerformed

    private void CloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CloseActionPerformed
        this.dispose();
    }//GEN-LAST:event_CloseActionPerformed

    private void Nombre2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Nombre2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Nombre2ActionPerformed

    public void cargarEdificios() {
        if (ciudad.E1.tipo != null && ciudad.E1.tipo.getClass() == Casa.class) {
            Edificio1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E2.tipo != null && ciudad.E2.tipo.getClass() == Casa.class) {
            Edificio5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E3.tipo != null && ciudad.E3.tipo.getClass() == Casa.class) {
            Edificio9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E4.tipo != null && ciudad.E4.tipo.getClass() == Casa.class) {
            Edificio13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E5.tipo != null && ciudad.E5.tipo.getClass() == Casa.class) {
            Edificio4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E6.tipo != null && ciudad.E6.tipo.getClass() == Casa.class) {
            Edificio17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E7.tipo != null && ciudad.E7.tipo.getClass() == Casa.class) {
            Edificio8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E8.tipo != null && ciudad.E8.tipo.getClass() == Casa.class) {
            Edificio21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E9.tipo != null && ciudad.E9.tipo.getClass() == Casa.class) {
            Edificio12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E10.tipo != null && ciudad.E10.tipo.getClass() == Casa.class) {
            Edificio25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E11.tipo != null && ciudad.E11.tipo.getClass() == Casa.class) {
            Edificio16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E12.tipo != null && ciudad.E12.tipo.getClass() == Casa.class) {
            Edificio29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E13.tipo != null && ciudad.E13.tipo.getClass() == Casa.class) {
            Edificio20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E14.tipo != null && ciudad.E14.tipo.getClass() == Casa.class) {
            Edificio33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E15.tipo != null && ciudad.E15.tipo.getClass() == Casa.class) {
            Edificio24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E16.tipo != null && ciudad.E16.tipo.getClass() == Casa.class) {
            Edificio28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E17.tipo != null && ciudad.E17.tipo.getClass() == Casa.class) {
            Edificio32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E18.tipo != null && ciudad.E18.tipo.getClass() == Casa.class) {
            Edificio36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E19.tipo != null && ciudad.E19.tipo.getClass() == Casa.class) {
            Edificio3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E20.tipo != null && ciudad.E20.tipo.getClass() == Casa.class) {
            Edificio15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E21.tipo != null && ciudad.E21.tipo.getClass() == Casa.class) {
            Edificio27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E22.tipo != null && ciudad.E22.tipo.getClass() == Casa.class) {
            Edificio2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E23.tipo != null && ciudad.E23.tipo.getClass() == Casa.class) {
            Edificio7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E24.tipo != null && ciudad.E24.tipo.getClass() == Casa.class) {
            Edificio14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E25.tipo != null && ciudad.E25.tipo.getClass() == Casa.class) {
            Edificio19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E26.tipo != null && ciudad.E26.tipo.getClass() == Casa.class) {
            Edificio26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E27.tipo != null && ciudad.E27.tipo.getClass() == Casa.class) {
            Edificio31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E28.tipo != null && ciudad.E28.tipo.getClass() == Casa.class) {
            Edificio6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E29.tipo != null && ciudad.E29.tipo.getClass() == Casa.class) {
            Edificio11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E30.tipo != null && ciudad.E30.tipo.getClass() == Casa.class) {
            Edificio18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E31.tipo != null && ciudad.E31.tipo.getClass() == Casa.class) {
            Edificio23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E32.tipo != null && ciudad.E32.tipo.getClass() == Casa.class) {
            Edificio30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E33.tipo != null && ciudad.E33.tipo.getClass() == Casa.class) {
            Edificio35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E34.tipo != null && ciudad.E34.tipo.getClass() == Casa.class) {
            Edificio10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E35.tipo != null && ciudad.E35.tipo.getClass() == Casa.class) {
            Edificio22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }
        if (ciudad.E36.tipo != null && ciudad.E36.tipo.getClass() == Casa.class) {
            Edificio34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
        }

        if (ciudad.E1.tipo != null && ciudad.E1.tipo.getClass() == Hospital.class) {
            Edificio1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E2.tipo != null && ciudad.E2.tipo.getClass() == Hospital.class) {
            Edificio5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E3.tipo != null && ciudad.E3.tipo.getClass() == Hospital.class) {
            Edificio9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E4.tipo != null && ciudad.E4.tipo.getClass() == Hospital.class) {
            Edificio13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E5.tipo != null && ciudad.E5.tipo.getClass() == Hospital.class) {
            Edificio4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E6.tipo != null && ciudad.E6.tipo.getClass() == Hospital.class) {
            Edificio17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E7.tipo != null && ciudad.E7.tipo.getClass() == Hospital.class) {
            Edificio8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E8.tipo != null && ciudad.E8.tipo.getClass() == Hospital.class) {
            Edificio21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E9.tipo != null && ciudad.E9.tipo.getClass() == Hospital.class) {
            Edificio12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E10.tipo != null && ciudad.E10.tipo.getClass() == Hospital.class) {
            Edificio25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E11.tipo != null && ciudad.E11.tipo.getClass() == Hospital.class) {
            Edificio16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E12.tipo != null && ciudad.E12.tipo.getClass() == Hospital.class) {
            Edificio29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E13.tipo != null && ciudad.E13.tipo.getClass() == Hospital.class) {
            Edificio20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E14.tipo != null && ciudad.E14.tipo.getClass() == Hospital.class) {
            Edificio33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E15.tipo != null && ciudad.E15.tipo.getClass() == Hospital.class) {
            Edificio24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E16.tipo != null && ciudad.E16.tipo.getClass() == Hospital.class) {
            Edificio28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E17.tipo != null && ciudad.E17.tipo.getClass() == Hospital.class) {
            Edificio32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E18.tipo != null && ciudad.E18.tipo.getClass() == Hospital.class) {
            Edificio36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E19.tipo != null && ciudad.E19.tipo.getClass() == Hospital.class) {
            Edificio3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E20.tipo != null && ciudad.E20.tipo.getClass() == Hospital.class) {
            Edificio15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E21.tipo != null && ciudad.E21.tipo.getClass() == Hospital.class) {
            Edificio27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E22.tipo != null && ciudad.E22.tipo.getClass() == Hospital.class) {
            Edificio2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E23.tipo != null && ciudad.E23.tipo.getClass() == Hospital.class) {
            Edificio7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E24.tipo != null && ciudad.E24.tipo.getClass() == Hospital.class) {
            Edificio14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E25.tipo != null && ciudad.E25.tipo.getClass() == Hospital.class) {
            Edificio19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E26.tipo != null && ciudad.E26.tipo.getClass() == Hospital.class) {
            Edificio26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E27.tipo != null && ciudad.E27.tipo.getClass() == Hospital.class) {
            Edificio31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E28.tipo != null && ciudad.E28.tipo.getClass() == Hospital.class) {
            Edificio6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E29.tipo != null && ciudad.E29.tipo.getClass() == Hospital.class) {
            Edificio11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E30.tipo != null && ciudad.E30.tipo.getClass() == Hospital.class) {
            Edificio18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E31.tipo != null && ciudad.E31.tipo.getClass() == Hospital.class) {
            Edificio23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E32.tipo != null && ciudad.E32.tipo.getClass() == Hospital.class) {
            Edificio30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E33.tipo != null && ciudad.E33.tipo.getClass() == Hospital.class) {
            Edificio35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E34.tipo != null && ciudad.E34.tipo.getClass() == Hospital.class) {
            Edificio10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E35.tipo != null && ciudad.E35.tipo.getClass() == Hospital.class) {
            Edificio22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }
        if (ciudad.E36.tipo != null && ciudad.E36.tipo.getClass() == Hospital.class) {
            Edificio34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
        }

        if (ciudad.E1.tipo != null && ciudad.E1.tipo.getClass() == Mall.class) {
            Edificio1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E2.tipo != null && ciudad.E2.tipo.getClass() == Mall.class) {
            Edificio5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E3.tipo != null && ciudad.E3.tipo.getClass() == Mall.class) {
            Edificio9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E4.tipo != null && ciudad.E4.tipo.getClass() == Mall.class) {
            Edificio13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E5.tipo != null && ciudad.E5.tipo.getClass() == Mall.class) {
            Edificio4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E6.tipo != null && ciudad.E6.tipo.getClass() == Mall.class) {
            Edificio17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E7.tipo != null && ciudad.E7.tipo.getClass() == Mall.class) {
            Edificio8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E8.tipo != null && ciudad.E8.tipo.getClass() == Mall.class) {
            Edificio21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E9.tipo != null && ciudad.E9.tipo.getClass() == Mall.class) {
            Edificio12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E10.tipo != null && ciudad.E10.tipo.getClass() == Mall.class) {
            Edificio25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E11.tipo != null && ciudad.E11.tipo.getClass() == Mall.class) {
            Edificio16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E12.tipo != null && ciudad.E12.tipo.getClass() == Mall.class) {
            Edificio29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E13.tipo != null && ciudad.E13.tipo.getClass() == Mall.class) {
            Edificio20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E14.tipo != null && ciudad.E14.tipo.getClass() == Mall.class) {
            Edificio33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E15.tipo != null && ciudad.E15.tipo.getClass() == Mall.class) {
            Edificio24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E16.tipo != null && ciudad.E16.tipo.getClass() == Mall.class) {
            Edificio28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E17.tipo != null && ciudad.E17.tipo.getClass() == Mall.class) {
            Edificio32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E18.tipo != null && ciudad.E18.tipo.getClass() == Mall.class) {
            Edificio36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E19.tipo != null && ciudad.E19.tipo.getClass() == Mall.class) {
            Edificio3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E20.tipo != null && ciudad.E20.tipo.getClass() == Mall.class) {
            Edificio15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E21.tipo != null && ciudad.E21.tipo.getClass() == Mall.class) {
            Edificio27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E22.tipo != null && ciudad.E22.tipo.getClass() == Mall.class) {
            Edificio2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E23.tipo != null && ciudad.E23.tipo.getClass() == Mall.class) {
            Edificio7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E24.tipo != null && ciudad.E24.tipo.getClass() == Mall.class) {
            Edificio14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E25.tipo != null && ciudad.E25.tipo.getClass() == Mall.class) {
            Edificio19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E26.tipo != null && ciudad.E26.tipo.getClass() == Mall.class) {
            Edificio26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E27.tipo != null && ciudad.E27.tipo.getClass() == Mall.class) {
            Edificio31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E28.tipo != null && ciudad.E28.tipo.getClass() == Mall.class) {
            Edificio6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E29.tipo != null && ciudad.E29.tipo.getClass() == Mall.class) {
            Edificio11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E30.tipo != null && ciudad.E30.tipo.getClass() == Mall.class) {
            Edificio18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E31.tipo != null && ciudad.E31.tipo.getClass() == Mall.class) {
            Edificio23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E32.tipo != null && ciudad.E32.tipo.getClass() == Mall.class) {
            Edificio30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E33.tipo != null && ciudad.E33.tipo.getClass() == Mall.class) {
            Edificio35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E34.tipo != null && ciudad.E34.tipo.getClass() == Mall.class) {
            Edificio10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E35.tipo != null && ciudad.E35.tipo.getClass() == Mall.class) {
            Edificio22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }
        if (ciudad.E36.tipo != null && ciudad.E36.tipo.getClass() == Mall.class) {
            Edificio34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
        }

        if (ciudad.E1.tipo != null && ciudad.E1.tipo.getClass() == Policia.class) {
            Edificio1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E2.tipo != null && ciudad.E2.tipo.getClass() == Policia.class) {
            Edificio5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E3.tipo != null && ciudad.E3.tipo.getClass() == Policia.class) {
            Edificio9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E4.tipo != null && ciudad.E4.tipo.getClass() == Policia.class) {
            Edificio13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E5.tipo != null && ciudad.E5.tipo.getClass() == Policia.class) {
            Edificio4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E6.tipo != null && ciudad.E6.tipo.getClass() == Policia.class) {
            Edificio17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E7.tipo != null && ciudad.E7.tipo.getClass() == Policia.class) {
            Edificio8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E8.tipo != null && ciudad.E8.tipo.getClass() == Policia.class) {
            Edificio21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E9.tipo != null && ciudad.E9.tipo.getClass() == Policia.class) {
            Edificio12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E10.tipo != null && ciudad.E10.tipo.getClass() == Policia.class) {
            Edificio25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E11.tipo != null && ciudad.E11.tipo.getClass() == Policia.class) {
            Edificio16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E12.tipo != null && ciudad.E12.tipo.getClass() == Policia.class) {
            Edificio29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E13.tipo != null && ciudad.E13.tipo.getClass() == Policia.class) {
            Edificio20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E14.tipo != null && ciudad.E14.tipo.getClass() == Policia.class) {
            Edificio33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E15.tipo != null && ciudad.E15.tipo.getClass() == Policia.class) {
            Edificio24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E16.tipo != null && ciudad.E16.tipo.getClass() == Policia.class) {
            Edificio28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E17.tipo != null && ciudad.E17.tipo.getClass() == Policia.class) {
            Edificio32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E18.tipo != null && ciudad.E18.tipo.getClass() == Policia.class) {
            Edificio36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E19.tipo != null && ciudad.E19.tipo.getClass() == Policia.class) {
            Edificio3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E20.tipo != null && ciudad.E20.tipo.getClass() == Policia.class) {
            Edificio15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E21.tipo != null && ciudad.E21.tipo.getClass() == Policia.class) {
            Edificio27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E22.tipo != null && ciudad.E22.tipo.getClass() == Policia.class) {
            Edificio2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E23.tipo != null && ciudad.E23.tipo.getClass() == Policia.class) {
            Edificio7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E24.tipo != null && ciudad.E24.tipo.getClass() == Policia.class) {
            Edificio14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E25.tipo != null && ciudad.E25.tipo.getClass() == Policia.class) {
            Edificio19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E26.tipo != null && ciudad.E26.tipo.getClass() == Policia.class) {
            Edificio26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E27.tipo != null && ciudad.E27.tipo.getClass() == Policia.class) {
            Edificio31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E28.tipo != null && ciudad.E28.tipo.getClass() == Policia.class) {
            Edificio6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E29.tipo != null && ciudad.E29.tipo.getClass() == Policia.class) {
            Edificio11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E30.tipo != null && ciudad.E30.tipo.getClass() == Policia.class) {
            Edificio18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E31.tipo != null && ciudad.E31.tipo.getClass() == Policia.class) {
            Edificio23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E32.tipo != null && ciudad.E32.tipo.getClass() == Policia.class) {
            Edificio30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E33.tipo != null && ciudad.E33.tipo.getClass() == Policia.class) {
            Edificio35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E34.tipo != null && ciudad.E34.tipo.getClass() == Policia.class) {
            Edificio10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E35.tipo != null && ciudad.E35.tipo.getClass() == Policia.class) {
            Edificio22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }
        if (ciudad.E36.tipo != null && ciudad.E36.tipo.getClass() == Policia.class) {
            Edificio34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
        }

        if (ciudad.E1.tipo != null && ciudad.E1.tipo.getClass() == Bomberos.class) {
            Edificio1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E2.tipo != null && ciudad.E2.tipo.getClass() == Bomberos.class) {
            Edificio5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E3.tipo != null && ciudad.E3.tipo.getClass() == Bomberos.class) {
            Edificio9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E4.tipo != null && ciudad.E4.tipo.getClass() == Bomberos.class) {
            Edificio13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E5.tipo != null && ciudad.E5.tipo.getClass() == Bomberos.class) {
            Edificio4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E6.tipo != null && ciudad.E6.tipo.getClass() == Bomberos.class) {
            Edificio17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E7.tipo != null && ciudad.E7.tipo.getClass() == Bomberos.class) {
            Edificio8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E8.tipo != null && ciudad.E8.tipo.getClass() == Bomberos.class) {
            Edificio21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E9.tipo != null && ciudad.E9.tipo.getClass() == Bomberos.class) {
            Edificio12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E10.tipo != null && ciudad.E10.tipo.getClass() == Bomberos.class) {
            Edificio25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E11.tipo != null && ciudad.E11.tipo.getClass() == Bomberos.class) {
            Edificio16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E12.tipo != null && ciudad.E12.tipo.getClass() == Bomberos.class) {
            Edificio29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E13.tipo != null && ciudad.E13.tipo.getClass() == Bomberos.class) {
            Edificio20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E14.tipo != null && ciudad.E14.tipo.getClass() == Bomberos.class) {
            Edificio33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E15.tipo != null && ciudad.E15.tipo.getClass() == Bomberos.class) {
            Edificio24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E16.tipo != null && ciudad.E16.tipo.getClass() == Bomberos.class) {
            Edificio28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E17.tipo != null && ciudad.E17.tipo.getClass() == Bomberos.class) {
            Edificio32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E18.tipo != null && ciudad.E18.tipo.getClass() == Bomberos.class) {
            Edificio36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E19.tipo != null && ciudad.E19.tipo.getClass() == Bomberos.class) {
            Edificio3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E20.tipo != null && ciudad.E20.tipo.getClass() == Bomberos.class) {
            Edificio15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E21.tipo != null && ciudad.E21.tipo.getClass() == Bomberos.class) {
            Edificio27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E22.tipo != null && ciudad.E22.tipo.getClass() == Bomberos.class) {
            Edificio2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E23.tipo != null && ciudad.E23.tipo.getClass() == Bomberos.class) {
            Edificio7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E24.tipo != null && ciudad.E24.tipo.getClass() == Bomberos.class) {
            Edificio14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E25.tipo != null && ciudad.E25.tipo.getClass() == Bomberos.class) {
            Edificio19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E26.tipo != null && ciudad.E26.tipo.getClass() == Bomberos.class) {
            Edificio26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E27.tipo != null && ciudad.E27.tipo.getClass() == Bomberos.class) {
            Edificio31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E28.tipo != null && ciudad.E28.tipo.getClass() == Bomberos.class) {
            Edificio6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E29.tipo != null && ciudad.E29.tipo.getClass() == Bomberos.class) {
            Edificio11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E30.tipo != null && ciudad.E30.tipo.getClass() == Bomberos.class) {
            Edificio18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E31.tipo != null && ciudad.E31.tipo.getClass() == Bomberos.class) {
            Edificio23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E32.tipo != null && ciudad.E32.tipo.getClass() == Bomberos.class) {
            Edificio30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E33.tipo != null && ciudad.E33.tipo.getClass() == Bomberos.class) {
            Edificio35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E34.tipo != null && ciudad.E34.tipo.getClass() == Bomberos.class) {
            Edificio10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E35.tipo != null && ciudad.E35.tipo.getClass() == Bomberos.class) {
            Edificio22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }
        if (ciudad.E36.tipo != null && ciudad.E36.tipo.getClass() == Bomberos.class) {
            Edificio34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
        }

        if (ciudad.E1.tipo != null && ciudad.E1.tipo.getClass() == CruzRoja.class) {
            Edificio1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E2.tipo != null && ciudad.E2.tipo.getClass() == CruzRoja.class) {
            Edificio5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E3.tipo != null && ciudad.E3.tipo.getClass() == CruzRoja.class) {
            Edificio9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E4.tipo != null && ciudad.E4.tipo.getClass() == CruzRoja.class) {
            Edificio13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E5.tipo != null && ciudad.E5.tipo.getClass() == CruzRoja.class) {
            Edificio4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E6.tipo != null && ciudad.E6.tipo.getClass() == CruzRoja.class) {
            Edificio17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E7.tipo != null && ciudad.E7.tipo.getClass() == CruzRoja.class) {
            Edificio8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E8.tipo != null && ciudad.E8.tipo.getClass() == CruzRoja.class) {
            Edificio21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E9.tipo != null && ciudad.E9.tipo.getClass() == CruzRoja.class) {
            Edificio12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E10.tipo != null && ciudad.E10.tipo.getClass() == CruzRoja.class) {
            Edificio25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E11.tipo != null && ciudad.E11.tipo.getClass() == CruzRoja.class) {
            Edificio16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E12.tipo != null && ciudad.E12.tipo.getClass() == CruzRoja.class) {
            Edificio29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E13.tipo != null && ciudad.E13.tipo.getClass() == CruzRoja.class) {
            Edificio20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E14.tipo != null && ciudad.E14.tipo.getClass() == CruzRoja.class) {
            Edificio33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E15.tipo != null && ciudad.E15.tipo.getClass() == CruzRoja.class) {
            Edificio24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E16.tipo != null && ciudad.E16.tipo.getClass() == CruzRoja.class) {
            Edificio28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E17.tipo != null && ciudad.E17.tipo.getClass() == CruzRoja.class) {
            Edificio32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E18.tipo != null && ciudad.E18.tipo.getClass() == CruzRoja.class) {
            Edificio36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E19.tipo != null && ciudad.E19.tipo.getClass() == CruzRoja.class) {
            Edificio3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E20.tipo != null && ciudad.E20.tipo.getClass() == CruzRoja.class) {
            Edificio15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E21.tipo != null && ciudad.E21.tipo.getClass() == CruzRoja.class) {
            Edificio27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E22.tipo != null && ciudad.E22.tipo.getClass() == CruzRoja.class) {
            Edificio2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E23.tipo != null && ciudad.E23.tipo.getClass() == CruzRoja.class) {
            Edificio7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E24.tipo != null && ciudad.E24.tipo.getClass() == CruzRoja.class) {
            Edificio14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E25.tipo != null && ciudad.E25.tipo.getClass() == CruzRoja.class) {
            Edificio19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E26.tipo != null && ciudad.E26.tipo.getClass() == CruzRoja.class) {
            Edificio26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E27.tipo != null && ciudad.E27.tipo.getClass() == CruzRoja.class) {
            Edificio31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E28.tipo != null && ciudad.E28.tipo.getClass() == CruzRoja.class) {
            Edificio6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E29.tipo != null && ciudad.E29.tipo.getClass() == CruzRoja.class) {
            Edificio11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E30.tipo != null && ciudad.E30.tipo.getClass() == CruzRoja.class) {
            Edificio18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E31.tipo != null && ciudad.E31.tipo.getClass() == CruzRoja.class) {
            Edificio23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E32.tipo != null && ciudad.E32.tipo.getClass() == CruzRoja.class) {
            Edificio30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E33.tipo != null && ciudad.E33.tipo.getClass() == CruzRoja.class) {
            Edificio35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E34.tipo != null && ciudad.E34.tipo.getClass() == CruzRoja.class) {
            Edificio10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E35.tipo != null && ciudad.E35.tipo.getClass() == CruzRoja.class) {
            Edificio22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
        if (ciudad.E36.tipo != null && ciudad.E36.tipo.getClass() == CruzRoja.class) {
            Edificio34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
        }
    }

    public void buscarDisponibles(Edificio aux) {
        if (aux == null) {
            return;
        }
        if (aux.tipo != null && aux.incidente == null) {
            edificiosDisponibles = true;
            return;
        }
        if (aux.calleDerecha != null) {
            buscarDisponibles(aux.calleDerecha.edificio);
        }
    }

    private void Edificio24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio24ActionPerformed
        if (ciudad.E15.incidente != null) {
            incidenteActual = ciudad.E15.incidente;
        }
        if (ciudad.E15.tipo != null) {
            if (ciudad.E15.incidente != null) {
                id.setText(ciudad.E15.incidente.Id + "");
                tipos.setText(ciudad.E15.incidente.tipo);
                nombre.setText(ciudad.E15.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio24ActionPerformed

    private void FuegoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FuegoActionPerformed
        int Id = generarId();
        String T = "Incendio";
        String No = "";
        int Tiempo = 0;

        edificiosDisponibles = false;
        buscarDisponibles(ciudad.E1);
        buscarDisponibles(ciudad.E22);
        buscarDisponibles(ciudad.E19);
        buscarDisponibles(ciudad.E4);
        buscarDisponibles(ciudad.E24);
        buscarDisponibles(ciudad.E20);
        buscarDisponibles(ciudad.E10);
        buscarDisponibles(ciudad.E26);
        buscarDisponibles(ciudad.E21);
        buscarDisponibles(ciudad.E16);
        if (edificiosDisponibles == false) {
            JOptionPane.showMessageDialog(null, "No hay edificios disponibles");
            return;
        }
        Random R = new Random();
        int EdificioRandom; // numero del edificio
        Edificio edificio;
        Random r = new Random();
        int N = r.nextInt(3) + 1; //numero del tipo de incidente
        while (true) {
            EdificioRandom = R.nextInt(36) + 1;
            if (buscarIncidente(EdificioRandom) != null) {
                edificio = buscarIncidente(EdificioRandom);
                break;
            }
        }
        if (N == 1) {
            Nombre1.setText("Level 1");
            Tiempo = 3;
            No = "Level 1";
        }
        if (N == 2) {
            Nombre1.setText("Level 2");
            Tiempo = 5;
            No = "Level 2";
        }
        if (N == 3) {
            Nombre1.setText("Level 3");
            Tiempo = 8;
            No = "Level 3";
        }
        edificio.incidente = new Incidente(Id, T, No, Tiempo);
        Incidente incidente = edificio.incidente;
        met.insertarIncidente(ciudad, incidente);
        crearIncidente();
    }//GEN-LAST:event_FuegoActionPerformed

    private void SaludActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SaludActionPerformed
        int Id = generarId();
        String T = "Salud";
        String No = "";
        int Tiempo = 0;

        edificiosDisponibles = false;
        buscarDisponibles(ciudad.E1);
        buscarDisponibles(ciudad.E22);
        buscarDisponibles(ciudad.E19);
        buscarDisponibles(ciudad.E4);
        buscarDisponibles(ciudad.E24);
        buscarDisponibles(ciudad.E20);
        buscarDisponibles(ciudad.E10);
        buscarDisponibles(ciudad.E26);
        buscarDisponibles(ciudad.E21);
        buscarDisponibles(ciudad.E16);
        if (edificiosDisponibles == false) {
            JOptionPane.showMessageDialog(null, "No hay edificios disponibles");
            return;
        }

        Random R = new Random();
        int EdificioRandom; // numero del edificio
        Edificio edificio;
        Random r = new Random();
        int N = r.nextInt(3) + 1; //numero del tipo de incidente
        while (true) {
            EdificioRandom = R.nextInt(36) + 1;
            if (buscarIncidente(EdificioRandom) != null) {
                edificio = buscarIncidente(EdificioRandom);
                break;
            }
        }
        if (N == 1) {
            Nombre2.setText("Cut");
            Tiempo = 3;
            No = "Cut";
        }
        if (N == 2) {
            Nombre2.setText("Crash");
            Tiempo = 5;
            No = "Crash";
        }
        if (N == 3) {
            Nombre2.setText("Heart Attack");
            Tiempo = 8;
            No = "Heart Attack";
        }
        edificio.incidente = new Incidente(Id, T, No, Tiempo);
        Incidente incidente = edificio.incidente;
        met.insertarIncidente(ciudad, incidente);
        crearIncidente();
    }//GEN-LAST:event_SaludActionPerformed

    private void CrimenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CrimenActionPerformed
        int Id = generarId();
        String T = "Delito";
        String No = "";
        int Tiempo = 0;

        edificiosDisponibles = false;
        buscarDisponibles(ciudad.E1);
        buscarDisponibles(ciudad.E22);
        buscarDisponibles(ciudad.E19);
        buscarDisponibles(ciudad.E4);
        buscarDisponibles(ciudad.E24);
        buscarDisponibles(ciudad.E20);
        buscarDisponibles(ciudad.E10);
        buscarDisponibles(ciudad.E26);
        buscarDisponibles(ciudad.E21);
        buscarDisponibles(ciudad.E16);
        if (edificiosDisponibles == false) {
            JOptionPane.showMessageDialog(null, "No hay edificios disponibles");
            return;
        }

        Random R = new Random();
        int EdificioRandom; // numero del edificio
        Edificio edificio;
        Random r = new Random();
        int N = r.nextInt(3) + 1; //numero del tipo de incidente
        while (true) {
            EdificioRandom = R.nextInt(36) + 1;
            if (buscarIncidente(EdificioRandom) != null) {
                edificio = buscarIncidente(EdificioRandom);
                break;
            }
        }
        if (N == 1) {
            Nombre3.setText("Attack");
            Tiempo = 3;
            No = "Attack";
        }
        if (N == 2) {
            Nombre3.setText("Rob");
            Tiempo = 5;
            No = "Rob";
        }
        if (N == 3) {
            Nombre3.setText("Murder");
            Tiempo = 8;
            No = "Murder";
        }
        edificio.incidente = new Incidente(Id, T, No, Tiempo);
        Incidente incidente = edificio.incidente;
        met.insertarIncidente(ciudad, incidente);
        crearIncidente();
    }//GEN-LAST:event_CrimenActionPerformed

    private void Edificio2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio2ActionPerformed
        if (ciudad.E22.incidente != null) {
            incidenteActual = ciudad.E22.incidente;
        }
        if (ciudad.E22.tipo != null) {
            if (ciudad.E22.incidente != null) {
                id.setText(ciudad.E22.incidente.Id + "");
                tipos.setText(ciudad.E22.incidente.tipo);
                nombre.setText(ciudad.E22.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio2ActionPerformed

    private void Edificio5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio5ActionPerformed
        if (ciudad.E2.incidente != null) {
            incidenteActual = ciudad.E2.incidente;
        }
        if (ciudad.E2.tipo != null) {
            if (ciudad.E2.incidente != null) {
                id.setText(ciudad.E2.incidente.Id + "");
                tipos.setText(ciudad.E2.incidente.tipo);
                nombre.setText(ciudad.E2.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio5ActionPerformed

    private void Edificio6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio6ActionPerformed
        if (ciudad.E28.incidente != null) {
            incidenteActual = ciudad.E28.incidente;
        }
        if (ciudad.E28.tipo != null) {
            if (ciudad.E28.incidente != null) {
                id.setText(ciudad.E28.incidente.Id + "");
                tipos.setText(ciudad.E28.incidente.tipo);
                nombre.setText(ciudad.E28.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio6ActionPerformed

    private void Edificio9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio9ActionPerformed
        if (ciudad.E3.incidente != null) {
            incidenteActual = ciudad.E3.incidente;
        }
        if (ciudad.E3.tipo != null) {
            if (ciudad.E3.incidente != null) {
                id.setText(ciudad.E3.incidente.Id + "");
                tipos.setText(ciudad.E3.incidente.tipo);
                nombre.setText(ciudad.E3.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio9ActionPerformed

    private void Edificio10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio10ActionPerformed
        if (ciudad.E34.incidente != null) {
            incidenteActual = ciudad.E34.incidente;
        }
        if (ciudad.E34.tipo != null) {
            if (ciudad.E34.incidente != null) {
                id.setText(ciudad.E34.incidente.Id + "");
                tipos.setText(ciudad.E34.incidente.tipo);
                nombre.setText(ciudad.E34.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio10ActionPerformed

    private void Edificio4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio4ActionPerformed
        if (ciudad.E5.incidente != null) {
            incidenteActual = ciudad.E5.incidente;
        }
        if (ciudad.E5.tipo != null) {
            if (ciudad.E5.incidente != null) {
                id.setText(ciudad.E5.incidente.Id + "");
                tipos.setText(ciudad.E5.incidente.tipo);
                nombre.setText(ciudad.E5.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio4ActionPerformed

    private void Edificio7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio7ActionPerformed
        if (ciudad.E23.incidente != null) {
            incidenteActual = ciudad.E23.incidente;
        }
        if (ciudad.E23.tipo != null) {
            if (ciudad.E23.incidente != null) {
                id.setText(ciudad.E23.incidente.Id + "");
                tipos.setText(ciudad.E23.incidente.tipo);
                nombre.setText(ciudad.E23.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio7ActionPerformed

    private void Edificio8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio8ActionPerformed
        if (ciudad.E7.incidente != null) {
            incidenteActual = ciudad.E7.incidente;
        }
        if (ciudad.E7.tipo != null) {
            if (ciudad.E7.incidente != null) {
                id.setText(ciudad.E7.incidente.Id + "");
                tipos.setText(ciudad.E7.incidente.tipo);
                nombre.setText(ciudad.E7.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio8ActionPerformed

    private void Edificio11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio11ActionPerformed
        if (ciudad.E29.incidente != null) {
            incidenteActual = ciudad.E29.incidente;
        }
        if (ciudad.E29.tipo != null) {
            if (ciudad.E29.incidente != null) {
                id.setText(ciudad.E29.incidente.Id + "");
                tipos.setText(ciudad.E29.incidente.tipo);
                nombre.setText(ciudad.E29.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio11ActionPerformed

    private void Edificio12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio12ActionPerformed
        if (ciudad.E9.incidente != null) {
            incidenteActual = ciudad.E9.incidente;
        }
        if (ciudad.E9.tipo != null) {
            if (ciudad.E9.incidente != null) {
                id.setText(ciudad.E9.incidente.Id + "");
                tipos.setText(ciudad.E9.incidente.tipo);
                nombre.setText(ciudad.E9.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio12ActionPerformed

    private void Edificio13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio13ActionPerformed
        if (ciudad.E4.incidente != null) {
            incidenteActual = ciudad.E4.incidente;
        }
        if (ciudad.E4.tipo != null) {
            if (ciudad.E4.incidente != null) {
                id.setText(ciudad.E4.incidente.Id + "");
                tipos.setText(ciudad.E4.incidente.tipo);
                nombre.setText(ciudad.E4.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio13ActionPerformed

    private void Edificio14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio14ActionPerformed
        if (ciudad.E24.incidente != null) {
            incidenteActual = ciudad.E24.incidente;
        }
        if (ciudad.E24.tipo != null) {
            if (ciudad.E24.incidente != null) {
                id.setText(ciudad.E24.incidente.Id + "");
                tipos.setText(ciudad.E24.incidente.tipo);
                nombre.setText(ciudad.E24.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio14ActionPerformed

    private void Edificio17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio17ActionPerformed
        if (ciudad.E6.incidente != null) {
            incidenteActual = ciudad.E6.incidente;
        }
        if (ciudad.E6.tipo != null) {
            if (ciudad.E6.incidente != null) {
                id.setText(ciudad.E6.incidente.Id + "");
                tipos.setText(ciudad.E6.incidente.tipo);
                nombre.setText(ciudad.E6.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio17ActionPerformed

    private void Edificio18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio18ActionPerformed
        if (ciudad.E30.incidente != null) {
            incidenteActual = ciudad.E30.incidente;
        }
        if (ciudad.E30.tipo != null) {
            if (ciudad.E30.incidente != null) {
                id.setText(ciudad.E30.incidente.Id + "");
                tipos.setText(ciudad.E30.incidente.tipo);
                nombre.setText(ciudad.E30.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio18ActionPerformed

    private void Edificio21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio21ActionPerformed
        if (ciudad.E8.incidente != null) {
            incidenteActual = ciudad.E8.incidente;
        }
        if (ciudad.E8.tipo != null) {
            if (ciudad.E8.incidente != null) {
                id.setText(ciudad.E8.incidente.Id + "");
                tipos.setText(ciudad.E8.incidente.tipo);
                nombre.setText(ciudad.E8.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio21ActionPerformed

    private void Edificio22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio22ActionPerformed
        if (ciudad.E35.incidente != null) {
            incidenteActual = ciudad.E35.incidente;
        }
        if (ciudad.E35.tipo != null) {
            if (ciudad.E35.incidente != null) {
                id.setText(ciudad.E35.incidente.Id + "");
                tipos.setText(ciudad.E35.incidente.tipo);
                nombre.setText(ciudad.E35.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio22ActionPerformed

    private void Edificio15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio15ActionPerformed
        if (ciudad.E20.incidente != null) {
            incidenteActual = ciudad.E20.incidente;
        }
        if (ciudad.E20.tipo != null) {
            if (ciudad.E20.incidente != null) {
                id.setText(ciudad.E20.incidente.Id + "");
                tipos.setText(ciudad.E20.incidente.tipo);
                nombre.setText(ciudad.E20.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio15ActionPerformed

    private void Edificio16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio16ActionPerformed
        if (ciudad.E11.incidente != null) {
            incidenteActual = ciudad.E11.incidente;
        }
        if (ciudad.E11.tipo != null) {
            if (ciudad.E11.incidente != null) {
                id.setText(ciudad.E11.incidente.Id + "");
                tipos.setText(ciudad.E11.incidente.tipo);
                nombre.setText(ciudad.E11.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio16ActionPerformed

    private void Edificio19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio19ActionPerformed
        if (ciudad.E25.incidente != null) {
            incidenteActual = ciudad.E25.incidente;
        }
        if (ciudad.E25.tipo != null) {
            if (ciudad.E25.incidente != null) {
                id.setText(ciudad.E25.incidente.Id + "");
                tipos.setText(ciudad.E25.incidente.tipo);
                nombre.setText(ciudad.E25.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio19ActionPerformed

    private void Edificio20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio20ActionPerformed
        if (ciudad.E13.incidente != null) {
            incidenteActual = ciudad.E13.incidente;
        }
        if (ciudad.E13.tipo != null) {
            if (ciudad.E13.incidente != null) {
                id.setText(ciudad.E13.incidente.Id + "");
                tipos.setText(ciudad.E13.incidente.tipo);
                nombre.setText(ciudad.E13.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio20ActionPerformed

    private void Edificio23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio23ActionPerformed
        if (ciudad.E31.incidente != null) {
            incidenteActual = ciudad.E31.incidente;
        }
        if (ciudad.E31.tipo != null) {
            if (ciudad.E31.incidente != null) {
                id.setText(ciudad.E31.incidente.Id + "");
                tipos.setText(ciudad.E31.incidente.tipo);
                nombre.setText(ciudad.E31.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio23ActionPerformed

    private void Edificio25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio25ActionPerformed
        if (ciudad.E10.incidente != null) {
            incidenteActual = ciudad.E10.incidente;
        }
        if (ciudad.E10.tipo != null) {
            if (ciudad.E10.incidente != null) {
                id.setText(ciudad.E10.incidente.Id + "");
                tipos.setText(ciudad.E10.incidente.tipo);
                nombre.setText(ciudad.E10.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio25ActionPerformed

    private void Edificio26ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio26ActionPerformed
        if (ciudad.E26.incidente != null) {
            incidenteActual = ciudad.E26.incidente;
        }
        if (ciudad.E26.tipo != null) {
            if (ciudad.E26.incidente != null) {
                id.setText(ciudad.E26.incidente.Id + "");
                tipos.setText(ciudad.E26.incidente.tipo);
                nombre.setText(ciudad.E26.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio26ActionPerformed

    private void Edificio29ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio29ActionPerformed
        if (ciudad.E12.incidente != null) {
            incidenteActual = ciudad.E12.incidente;
        }
        if (ciudad.E12.tipo != null) {
            if (ciudad.E12.incidente != null) {
                id.setText(ciudad.E12.incidente.Id + "");
                tipos.setText(ciudad.E12.incidente.tipo);
                nombre.setText(ciudad.E12.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio29ActionPerformed

    private void Edificio30ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio30ActionPerformed
        if (ciudad.E32.incidente != null) {
            incidenteActual = ciudad.E32.incidente;
        }
        if (ciudad.E32.tipo != null) {
            if (ciudad.E32.incidente != null) {
                id.setText(ciudad.E32.incidente.Id + "");
                tipos.setText(ciudad.E32.incidente.tipo);
                nombre.setText(ciudad.E32.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio30ActionPerformed

    private void Edificio33ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio33ActionPerformed
        if (ciudad.E14.incidente != null) {
            incidenteActual = ciudad.E14.incidente;
        }
        if (ciudad.E14.tipo != null) {
            if (ciudad.E14.incidente != null) {
                id.setText(ciudad.E14.incidente.Id + "");
                tipos.setText(ciudad.E14.incidente.tipo);
                nombre.setText(ciudad.E14.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio33ActionPerformed

    private void Edificio34ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio34ActionPerformed
        if (ciudad.E36.incidente != null) {
            incidenteActual = ciudad.E36.incidente;
        }
        if (ciudad.E36.tipo != null) {
            if (ciudad.E36.incidente != null) {
                id.setText(ciudad.E36.incidente.Id + "");
                tipos.setText(ciudad.E36.incidente.tipo);
                nombre.setText(ciudad.E36.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio34ActionPerformed

    private void Edificio27ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio27ActionPerformed
        if (ciudad.E21.incidente != null) {
            incidenteActual = ciudad.E21.incidente;
        }
        if (ciudad.E21.tipo != null) {
            if (ciudad.E21.incidente != null) {
                id.setText(ciudad.E21.incidente.Id + "");
                tipos.setText(ciudad.E21.incidente.tipo);
                nombre.setText(ciudad.E21.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio27ActionPerformed

    private void Edificio28ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio28ActionPerformed
        if (ciudad.E16.incidente != null) {
            incidenteActual = ciudad.E16.incidente;
        }
        if (ciudad.E16.tipo != null) {
            if (ciudad.E16.incidente != null) {
                id.setText(ciudad.E16.incidente.Id + "");
                tipos.setText(ciudad.E16.incidente.tipo);
                nombre.setText(ciudad.E16.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio28ActionPerformed

    private void Edificio31ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio31ActionPerformed
        if (ciudad.E27.incidente != null) {
            incidenteActual = ciudad.E27.incidente;
        }
        if (ciudad.E27.tipo != null) {
            if (ciudad.E27.incidente != null) {
                id.setText(ciudad.E27.incidente.Id + "");
                tipos.setText(ciudad.E27.incidente.tipo);
                nombre.setText(ciudad.E27.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio31ActionPerformed

    private void Edificio32ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio32ActionPerformed
        if (ciudad.E17.incidente != null) {
            incidenteActual = ciudad.E17.incidente;
        }
        if (ciudad.E17.tipo != null) {
            if (ciudad.E17.incidente != null) {
                id.setText(ciudad.E17.incidente.Id + "");
                tipos.setText(ciudad.E17.incidente.tipo);
                nombre.setText(ciudad.E17.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio32ActionPerformed

    private void Edificio35ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio35ActionPerformed
        if (ciudad.E33.incidente != null) {
            incidenteActual = ciudad.E33.incidente;
        }
        if (ciudad.E33.tipo != null) {
            if (ciudad.E33.incidente != null) {
                id.setText(ciudad.E33.incidente.Id + "");
                tipos.setText(ciudad.E33.incidente.tipo);
                nombre.setText(ciudad.E33.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio35ActionPerformed

    private void Edificio36ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio36ActionPerformed
        if (ciudad.E18.incidente != null) {
            incidenteActual = ciudad.E18.incidente;
        }
        if (ciudad.E18.tipo != null) {
            if (ciudad.E18.incidente != null) {
                id.setText(ciudad.E18.incidente.Id + "");
                tipos.setText(ciudad.E18.incidente.tipo);
                nombre.setText(ciudad.E18.incidente.nombre);
            }
        }
    }//GEN-LAST:event_Edificio36ActionPerformed

    private void ModificarIncidenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ModificarIncidenteActionPerformed
        String nuevoNombre = NuevoNombreIncidente.getText();
        Incidente aux = ciudad.raizIncidente;
        while (aux != null) {
            if (aux.equals(incidenteActual)) {
                incidenteActual.nombre = nuevoNombre;
                nombre.setText(nuevoNombre);
            }
            aux = aux.der;
        }
    }//GEN-LAST:event_ModificarIncidenteActionPerformed

    private void EliminarIncidenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EliminarIncidenteActionPerformed
        eliminarIncidente(incidenteActual.Id);
        
    }//GEN-LAST:event_EliminarIncidenteActionPerformed

    private void NuevoNombreIncidenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NuevoNombreIncidenteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NuevoNombreIncidenteActionPerformed

    public int generarId() {
        int Id = 1;
        Incidente aux = ciudad.raizIncidente;
        while (aux != null) {
            Id = aux.Id + 1;
            aux = aux.der;
        }
        return Id;
    }

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Atras;
    private javax.swing.JButton Close;
    private javax.swing.JButton Crimen;
    private javax.swing.JButton Edificio1;
    private javax.swing.JButton Edificio10;
    private javax.swing.JButton Edificio11;
    private javax.swing.JButton Edificio12;
    private javax.swing.JButton Edificio13;
    private javax.swing.JButton Edificio14;
    private javax.swing.JButton Edificio15;
    private javax.swing.JButton Edificio16;
    private javax.swing.JButton Edificio17;
    private javax.swing.JButton Edificio18;
    private javax.swing.JButton Edificio19;
    private javax.swing.JButton Edificio2;
    private javax.swing.JButton Edificio20;
    private javax.swing.JButton Edificio21;
    private javax.swing.JButton Edificio22;
    private javax.swing.JButton Edificio23;
    private javax.swing.JButton Edificio24;
    private javax.swing.JButton Edificio25;
    private javax.swing.JButton Edificio26;
    private javax.swing.JButton Edificio27;
    private javax.swing.JButton Edificio28;
    private javax.swing.JButton Edificio29;
    private javax.swing.JButton Edificio3;
    private javax.swing.JButton Edificio30;
    private javax.swing.JButton Edificio31;
    private javax.swing.JButton Edificio32;
    private javax.swing.JButton Edificio33;
    private javax.swing.JButton Edificio34;
    private javax.swing.JButton Edificio35;
    private javax.swing.JButton Edificio36;
    private javax.swing.JButton Edificio4;
    private javax.swing.JButton Edificio5;
    private javax.swing.JButton Edificio6;
    private javax.swing.JButton Edificio7;
    private javax.swing.JButton Edificio8;
    private javax.swing.JButton Edificio9;
    private javax.swing.JButton EliminarIncidente;
    private javax.swing.JLabel FondoSImular;
    private javax.swing.JButton Fuego;
    private javax.swing.JButton Guardar;
    private javax.swing.JLabel Insidente1;
    private javax.swing.JLabel Insidente10;
    private javax.swing.JLabel Insidente11;
    private javax.swing.JLabel Insidente12;
    private javax.swing.JLabel Insidente13;
    private javax.swing.JLabel Insidente14;
    private javax.swing.JLabel Insidente15;
    private javax.swing.JLabel Insidente16;
    private javax.swing.JLabel Insidente17;
    private javax.swing.JLabel Insidente18;
    private javax.swing.JLabel Insidente19;
    private javax.swing.JLabel Insidente2;
    private javax.swing.JLabel Insidente20;
    private javax.swing.JLabel Insidente21;
    private javax.swing.JLabel Insidente22;
    private javax.swing.JLabel Insidente23;
    private javax.swing.JLabel Insidente24;
    private javax.swing.JLabel Insidente25;
    private javax.swing.JLabel Insidente26;
    private javax.swing.JLabel Insidente27;
    private javax.swing.JLabel Insidente28;
    private javax.swing.JLabel Insidente29;
    private javax.swing.JLabel Insidente3;
    private javax.swing.JLabel Insidente30;
    private javax.swing.JLabel Insidente31;
    private javax.swing.JLabel Insidente32;
    private javax.swing.JLabel Insidente33;
    private javax.swing.JLabel Insidente34;
    private javax.swing.JLabel Insidente35;
    private javax.swing.JLabel Insidente36;
    private javax.swing.JLabel Insidente4;
    private javax.swing.JLabel Insidente5;
    private javax.swing.JLabel Insidente6;
    private javax.swing.JLabel Insidente7;
    private javax.swing.JLabel Insidente8;
    private javax.swing.JLabel Insidente9;
    private javax.swing.JButton ModificarIncidente;
    private javax.swing.JTextField Nombre1;
    private javax.swing.JTextField Nombre2;
    private javax.swing.JTextField Nombre3;
    private javax.swing.JLabel NombreCiudad;
    private javax.swing.JTextField NuevoNombreIncidente;
    private javax.swing.JButton Salud;
    private javax.swing.JLabel Vehiculos;
    private javax.swing.JLabel id;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel nombre;
    private javax.swing.JLabel tipos;
    // End of variables declaration//GEN-END:variables

    //Retorna un edificio que no tenga incidente
    public Edificio buscarIncidente(int random) {
        if (random == 1) {
            if (ciudad.E1.incidente == null && ciudad.E1.tipo != null) {
                return ciudad.E1;
            }
        }
        if (random == 2) {
            if (ciudad.E2.incidente == null && ciudad.E2.tipo != null) {
                return ciudad.E2;
            }
        }
        if (random == 3) {
            if (ciudad.E3.incidente == null && ciudad.E3.tipo != null) {
                return ciudad.E3;
            }
        }
        if (random == 4) {
            if (ciudad.E4.incidente == null && ciudad.E4.tipo != null) {
                return ciudad.E4;
            }
        }
        if (random == 5) {
            if (ciudad.E5.incidente == null && ciudad.E5.tipo != null) {
                return ciudad.E5;
            }
        }
        if (random == 6) {
            if (ciudad.E6.incidente == null && ciudad.E6.tipo != null) {
                return ciudad.E6;
            }
        }
        if (random == 7) {
            if (ciudad.E7.incidente == null && ciudad.E7.tipo != null) {
                return ciudad.E7;
            }
        }
        if (random == 8) {
            if (ciudad.E8.incidente == null && ciudad.E8.tipo != null) {
                return ciudad.E8;
            }
        }
        if (random == 9) {
            if (ciudad.E9.incidente == null && ciudad.E9.tipo != null) {
                return ciudad.E9;
            }
        }
        if (random == 10) {
            if (ciudad.E10.incidente == null && ciudad.E10.tipo != null) {
                return ciudad.E10;
            }
        }
        if (random == 11) {
            if (ciudad.E11.incidente == null && ciudad.E11.tipo != null) {
                return ciudad.E11;
            }
        }
        if (random == 12) {
            if (ciudad.E12.incidente == null && ciudad.E12.tipo != null) {
                return ciudad.E12;
            }
        }
        if (random == 13) {
            if (ciudad.E13.incidente == null && ciudad.E13.tipo != null) {
                return ciudad.E13;
            }
        }
        if (random == 14) {
            if (ciudad.E14.incidente == null && ciudad.E14.tipo != null) {
                return ciudad.E14;
            }
        }
        if (random == 15) {
            if (ciudad.E15.incidente == null && ciudad.E15.tipo != null) {
                return ciudad.E15;
            }
        }
        if (random == 16) {
            if (ciudad.E16.incidente == null && ciudad.E16.tipo != null) {
                return ciudad.E16;
            }
        }
        if (random == 17) {
            if (ciudad.E17.incidente == null && ciudad.E17.tipo != null) {
                return ciudad.E17;
            }
        }
        if (random == 18) {
            if (ciudad.E18.incidente == null && ciudad.E18.tipo != null) {
                return ciudad.E18;
            }
        }
        if (random == 19) {
            if (ciudad.E19.incidente == null && ciudad.E19.tipo != null) {
                return ciudad.E19;
            }
        }
        if (random == 20) {
            if (ciudad.E20.incidente == null && ciudad.E20.tipo != null) {
                return ciudad.E20;
            }
        }
        if (random == 21) {
            if (ciudad.E21.incidente == null && ciudad.E21.tipo != null) {
                return ciudad.E21;
            }
        }
        if (random == 22) {
            if (ciudad.E22.incidente == null && ciudad.E22.tipo != null) {
                return ciudad.E22;
            }
        }
        if (random == 23) {
            if (ciudad.E23.incidente == null && ciudad.E23.tipo != null) {
                return ciudad.E23;
            }
        }
        if (random == 24) {
            if (ciudad.E24.incidente == null && ciudad.E24.tipo != null) {
                return ciudad.E24;
            }
        }
        if (random == 25) {
            if (ciudad.E25.incidente == null && ciudad.E25.tipo != null) {
                return ciudad.E25;
            }
        }
        if (random == 26) {
            if (ciudad.E26.incidente == null && ciudad.E26.tipo != null) {
                return ciudad.E26;
            }
        }
        if (random == 27) {
            if (ciudad.E27.incidente == null && ciudad.E27.tipo != null) {
                return ciudad.E27;
            }
        }
        if (random == 28) {
            if (ciudad.E28.incidente == null && ciudad.E28.tipo != null) {
                return ciudad.E28;
            }
        }
        if (random == 29) {
            if (ciudad.E29.incidente == null && ciudad.E29.tipo != null) {
                return ciudad.E29;
            }
        }
        if (random == 30) {
            if (ciudad.E30.incidente == null && ciudad.E30.tipo != null) {
                return ciudad.E30;
            }
        }
        if (random == 31) {
            if (ciudad.E31.incidente == null && ciudad.E31.tipo != null) {
                return ciudad.E31;
            }
        }
        if (random == 32) {
            if (ciudad.E32.incidente == null && ciudad.E32.tipo != null) {
                return ciudad.E32;
            }
        }
        if (random == 33) {
            if (ciudad.E33.incidente == null && ciudad.E33.tipo != null) {
                return ciudad.E33;
            }
        }
        if (random == 34) {
            if (ciudad.E34.incidente == null && ciudad.E34.tipo != null) {
                return ciudad.E34;
            }
        }
        if (random == 35) {
            if (ciudad.E35.incidente == null && ciudad.E35.tipo != null) {
                return ciudad.E35;
            }
        }
        if (random == 36) {
            if (ciudad.E36.incidente == null && ciudad.E36.tipo != null) {
                return ciudad.E36;
            }
        }
        return null;
    }

    public void crearIncidente() {
        if (ciudad.E1.incidente != null) {
            if (ciudad.E1.incidente.tipo.equals("Incendio")) {
                Insidente1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E1.incidente.tipo.equals("Salud")) {
                Insidente1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E1.incidente.tipo.equals("Delito")) {
                Insidente1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E2.incidente != null) {
            if (ciudad.E2.incidente.tipo.equals("Incendio")) {
                Insidente5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E2.incidente.tipo.equals("Salud")) {
                Insidente5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E2.incidente.tipo.equals("Delito")) {
                Insidente5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E3.incidente != null) {
            if (ciudad.E3.incidente.tipo.equals("Incendio")) {
                Insidente9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E3.incidente.tipo.equals("Salud")) {
                Insidente9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E3.incidente.tipo.equals("Delito")) {
                Insidente9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E4.incidente != null) {
            if (ciudad.E4.incidente.tipo.equals("Incendio")) {
                Insidente13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E4.incidente.tipo.equals("Salud")) {
                Insidente13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E4.incidente.tipo.equals("Delito")) {
                Insidente13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E5.incidente != null) {
            if (ciudad.E5.incidente.tipo.equals("Incendio")) {
                Insidente4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E5.incidente.tipo.equals("Salud")) {
                Insidente4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E5.incidente.tipo.equals("Delito")) {
                Insidente4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E6.incidente != null) {
            if (ciudad.E6.incidente.tipo.equals("Incendio")) {
                Insidente17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E6.incidente.tipo.equals("Salud")) {
                Insidente17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E6.incidente.tipo.equals("Delito")) {
                Insidente17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E7.incidente != null) {
            if (ciudad.E7.incidente.tipo.equals("Incendio")) {
                Insidente8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E7.incidente.tipo.equals("Salud")) {
                Insidente8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E7.incidente.tipo.equals("Delito")) {
                Insidente8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E8.incidente != null) {
            if (ciudad.E8.incidente.tipo.equals("Incendio")) {
                Insidente21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E8.incidente.tipo.equals("Salud")) {
                Insidente21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E8.incidente.tipo.equals("Delito")) {
                Insidente21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E9.incidente != null) {
            if (ciudad.E9.incidente.tipo.equals("Incendio")) {
                Insidente12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E9.incidente.tipo.equals("Salud")) {
                Insidente12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E9.incidente.tipo.equals("Delito")) {
                Insidente12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E10.incidente != null) {
            if (ciudad.E10.incidente.tipo.equals("Incendio")) {
                Insidente25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E10.incidente.tipo.equals("Salud")) {
                Insidente25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E10.incidente.tipo.equals("Delito")) {
                Insidente25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E11.incidente != null) {
            if (ciudad.E11.incidente.tipo.equals("Incendio")) {
                Insidente16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E11.incidente.tipo.equals("Salud")) {
                Insidente16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E11.incidente.tipo.equals("Delito")) {
                Insidente16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E12.incidente != null) {
            if (ciudad.E12.incidente.tipo.equals("Incendio")) {
                Insidente29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E12.incidente.tipo.equals("Salud")) {
                Insidente29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E12.incidente.tipo.equals("Delito")) {
                Insidente29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E13.incidente != null) {
            if (ciudad.E13.incidente.tipo.equals("Incendio")) {
                Insidente20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E13.incidente.tipo.equals("Salud")) {
                Insidente20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E13.incidente.tipo.equals("Delito")) {
                Insidente20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E14.incidente != null) {
            if (ciudad.E14.incidente.tipo.equals("Incendio")) {
                Insidente33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E14.incidente.tipo.equals("Salud")) {
                Insidente33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E14.incidente.tipo.equals("Delito")) {
                Insidente33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E15.incidente != null) {
            if (ciudad.E15.incidente.tipo.equals("Incendio")) {
                Insidente24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E15.incidente.tipo.equals("Salud")) {
                Insidente24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E15.incidente.tipo.equals("Delito")) {
                Insidente24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E16.incidente != null) {
            if (ciudad.E16.incidente.tipo.equals("Incendio")) {
                Insidente28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E16.incidente.tipo.equals("Salud")) {
                Insidente28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E16.incidente.tipo.equals("Delito")) {
                Insidente28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E17.incidente != null) {
            if (ciudad.E17.incidente.tipo.equals("Incendio")) {
                Insidente32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E17.incidente.tipo.equals("Salud")) {
                Insidente32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E17.incidente.tipo.equals("Delito")) {
                Insidente32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E18.incidente != null) {
            if (ciudad.E18.incidente.tipo.equals("Incendio")) {
                Insidente26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E18.incidente.tipo.equals("Salud")) {
                Insidente26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E18.incidente.tipo.equals("Delito")) {
                Insidente26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E19.incidente != null) {
            if (ciudad.E19.incidente.tipo.equals("Incendio")) {
                Insidente3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E19.incidente.tipo.equals("Salud")) {
                Insidente3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E19.incidente.tipo.equals("Delito")) {
                Insidente3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E20.incidente != null) {
            if (ciudad.E20.incidente.tipo.equals("Incendio")) {
                Insidente15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E20.incidente.tipo.equals("Salud")) {
                Insidente15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E20.incidente.tipo.equals("Delito")) {
                Insidente15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E21.incidente != null) {
            if (ciudad.E21.incidente.tipo.equals("Incendio")) {
                Insidente27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E21.incidente.tipo.equals("Salud")) {
                Insidente27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E21.incidente.tipo.equals("Delito")) {
                Insidente27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E22.incidente != null) {
            if (ciudad.E22.incidente.tipo.equals("Incendio")) {
                Insidente2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E22.incidente.tipo.equals("Salud")) {
                Insidente2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E22.incidente.tipo.equals("Delito")) {
                Insidente2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E23.incidente != null) {
            if (ciudad.E23.incidente.tipo.equals("Incendio")) {
                Insidente7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E23.incidente.tipo.equals("Salud")) {
                Insidente7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E23.incidente.tipo.equals("Delito")) {
                Insidente7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E24.incidente != null) {
            if (ciudad.E24.incidente.tipo.equals("Incendio")) {
                Insidente14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E24.incidente.tipo.equals("Salud")) {
                Insidente14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E24.incidente.tipo.equals("Delito")) {
                Insidente14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E25.incidente != null) {
            if (ciudad.E25.incidente.tipo.equals("Incendio")) {
                Insidente19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E25.incidente.tipo.equals("Salud")) {
                Insidente19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E25.incidente.tipo.equals("Delito")) {
                Insidente19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E26.incidente != null) {
            if (ciudad.E26.incidente.tipo.equals("Incendio")) {
                Insidente26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E26.incidente.tipo.equals("Salud")) {
                Insidente26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E26.incidente.tipo.equals("Delito")) {
                Insidente26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E27.incidente != null) {
            if (ciudad.E27.incidente.tipo.equals("Incendio")) {
                Insidente31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E27.incidente.tipo.equals("Salud")) {
                Insidente31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E27.incidente.tipo.equals("Delito")) {
                Insidente31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E28.incidente != null) {
            if (ciudad.E28.incidente.tipo.equals("Incendio")) {
                Insidente6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E28.incidente.tipo.equals("Salud")) {
                Insidente6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E28.incidente.tipo.equals("Delito")) {
                Insidente6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E29.incidente != null) {
            if (ciudad.E29.incidente.tipo.equals("Incendio")) {
                Insidente11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E29.incidente.tipo.equals("Salud")) {
                Insidente11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E29.incidente.tipo.equals("Delito")) {
                Insidente11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E30.incidente != null) {
            if (ciudad.E30.incidente.tipo.equals("Incendio")) {
                Insidente18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E30.incidente.tipo.equals("Salud")) {
                Insidente18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E30.incidente.tipo.equals("Delito")) {
                Insidente18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E31.incidente != null) {
            if (ciudad.E31.incidente.tipo.equals("Incendio")) {
                Insidente23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E31.incidente.tipo.equals("Salud")) {
                Insidente23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E31.incidente.tipo.equals("Delito")) {
                Insidente23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E32.incidente != null) {
            if (ciudad.E32.incidente.tipo.equals("Incendio")) {
                Insidente30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E32.incidente.tipo.equals("Salud")) {
                Insidente30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E32.incidente.tipo.equals("Delito")) {
                Insidente30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E33.incidente != null) {
            if (ciudad.E33.incidente.tipo.equals("Incendio")) {
                Insidente35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E33.incidente.tipo.equals("Salud")) {
                Insidente35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E33.incidente.tipo.equals("Delito")) {
                Insidente35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }

        }
        if (ciudad.E34.incidente != null) {
            if (ciudad.E34.incidente.tipo.equals("Incendio")) {
                Insidente10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E34.incidente.tipo.equals("Salud")) {
                Insidente10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E34.incidente.tipo.equals("Delito")) {
                Insidente10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E35.incidente != null) {
            if (ciudad.E35.incidente.tipo.equals("Incendio")) {
                Insidente22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E35.incidente.tipo.equals("Salud")) {
                Insidente22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E35.incidente.tipo.equals("Delito")) {
                Insidente22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
        if (ciudad.E36.incidente != null) {
            if (ciudad.E36.incidente.tipo.equals("Incendio")) {
                Insidente34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fuego (1).gif")));
            }
            if (ciudad.E36.incidente.tipo.equals("Salud")) {
                Insidente34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Emergencia (1).gif")));
            }
            if (ciudad.E36.incidente.tipo.equals("Delito")) {
                Insidente34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/POLICIA (1).gif")));
            }
        }
    }

    public void limpiarSimulaciones() {
        ciudad.E1.incidente = null;
        ciudad.E2.incidente = null;
        ciudad.E3.incidente = null;
        ciudad.E4.incidente = null;
        ciudad.E5.incidente = null;
        ciudad.E6.incidente = null;
        ciudad.E7.incidente = null;
        ciudad.E8.incidente = null;
        ciudad.E9.incidente = null;
        ciudad.E10.incidente = null;
        ciudad.E11.incidente = null;
        ciudad.E12.incidente = null;
        ciudad.E13.incidente = null;
        ciudad.E14.incidente = null;
        ciudad.E15.incidente = null;
        ciudad.E16.incidente = null;
        ciudad.E17.incidente = null;
        ciudad.E18.incidente = null;
        ciudad.E19.incidente = null;
        ciudad.E20.incidente = null;
        ciudad.E21.incidente = null;
        ciudad.E22.incidente = null;
        ciudad.E23.incidente = null;
        ciudad.E24.incidente = null;
        ciudad.E25.incidente = null;
        ciudad.E26.incidente = null;
        ciudad.E27.incidente = null;
        ciudad.E28.incidente = null;
        ciudad.E29.incidente = null;
        ciudad.E30.incidente = null;
        ciudad.E31.incidente = null;
        ciudad.E32.incidente = null;
        ciudad.E33.incidente = null;
        ciudad.E34.incidente = null;
        ciudad.E35.incidente = null;
        ciudad.E36.incidente = null;
    }

    public void simulacionFuego(Edificio edificio) {

    }

    

    public boolean eliminarIncidente(int d) {
        Incidente raiz = ciudad.raizIncidente;
        Incidente auxiliar = raiz;
        Incidente padre = raiz;
        boolean esHijoIzq = true;
        while (auxiliar.Id != d) {
            padre = auxiliar;
            if (d < auxiliar.Id) {
                esHijoIzq = true;
                auxiliar = auxiliar.Izq;
            } else {
                esHijoIzq = false;
                auxiliar = auxiliar.der;
            }
            if (auxiliar == null) {
                return false; // no lo encontro
            }
        }//fin del while
        if (auxiliar.Izq == null && auxiliar.der == null) {
            if (auxiliar == raiz) {
                raiz = null;
            } else if (esHijoIzq) {
                padre.Izq = null;
            } else {
                padre.der = null;
            }
        } else if (auxiliar.der == null) {
            if (auxiliar == raiz) {
                raiz = auxiliar.Izq;
            } else if (esHijoIzq) {
                padre.Izq = auxiliar.Izq;
            } else {
                padre.der = auxiliar.Izq;
            }
        } else if (auxiliar.Izq == null) {
            if (auxiliar == raiz) {
                raiz = auxiliar.der;
            } else if (esHijoIzq) {
                padre.Izq = auxiliar.der;
            } else {
                padre.der = auxiliar.Izq;
            }
        } else {
            Incidente reemplazo = obtenerUsuarioReemplazo(auxiliar);
            if (auxiliar == raiz) {
                raiz = reemplazo;
            } else if (esHijoIzq) {
                padre.Izq = reemplazo;
            } else {
                padre.der = reemplazo;
            }
            reemplazo.Izq = auxiliar.Izq;
        }
        return true;
    }

    //Metodo encargado de devolver el nodo reemplazo
    public Incidente obtenerUsuarioReemplazo(Incidente nodoReemp) {
        Incidente reemplazarPadre = nodoReemp;
        Incidente reemplazo = nodoReemp;
        Incidente auxiliar = nodoReemp.der;
        while (auxiliar != null) {
            reemplazarPadre = reemplazo;
            reemplazo = auxiliar;
            auxiliar = auxiliar.Izq;
        }
        if (reemplazo != nodoReemp.der) {
            reemplazarPadre.Izq = reemplazo.der;
            reemplazo.der = nodoReemp.der;
        }
        return reemplazo;
    }

}
