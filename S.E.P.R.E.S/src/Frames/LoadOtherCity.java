package Frames;

import Clases.Ciudad;
import Clases.Usuario;
import Metodos.Metodos;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class LoadOtherCity extends javax.swing.JFrame {

    Login login;
    Metodos met;
    Ciudad city;
    Menu menu;

    public LoadOtherCity(Metodos met, Login login,Menu menu) {
        initComponents();
        this.setTitle("Load Game");
        this.setResizable(false);
        this.setSize(490, 319);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.login = login;
        this.met = met;
        this.menu = menu;
        cargarCiudades(met.raiz);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        Close1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(null);

        jComboBox1.setBackground(new java.awt.Color(153, 7, 0));
        jComboBox1.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jComboBox1.setForeground(new java.awt.Color(255, 255, 255));
        jComboBox1.setBorder(null);
        jComboBox1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        getContentPane().add(jComboBox1);
        jComboBox1.setBounds(140, 200, 230, 50);

        jButton1.setBackground(new java.awt.Color(153, 7, 0));
        jButton1.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 48)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("LOAD CITY");
        jButton1.setBorder(null);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1);
        jButton1.setBounds(140, 110, 230, 60);

        Close1.setForeground(new java.awt.Color(0, 0, 0));
        Close1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/close_opt1.png"))); // NOI18N
        Close1.setToolTipText("Sign off");
        Close1.setBorder(null);
        Close1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Close1ActionPerformed(evt);
            }
        });
        getContentPane().add(Close1);
        Close1.setBounds(420, 0, 70, 70);

        jLabel1.setBackground(new java.awt.Color(153, 7, 0));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Menu1.jpg"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(-450, -60, 1040, 450);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        String ciudad = jComboBox1.getSelectedItem().toString();
        buscarCiudad(met.raiz, ciudad);
        Simulacion frame = new Simulacion(login, met, city);
        frame.setVisible(true);
        this.dispose();
        menu.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void Close1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Close1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_Close1ActionPerformed

    public void cargarCiudades(Usuario U) {
        if (U != null) {
            cargarCiudades(U.izq);
            cargarCiudades(U.der);
            Ciudad c = U.ciudad;
            while (c != null) {
                jComboBox1.addItem(U.ciudad.nombre);
                c = c.sig;
            }
        }
    }

    public Ciudad buscarCiudad(Usuario u, String ciudad) {
        if (u != null) {
            buscarCiudad(u.izq, ciudad);
            buscarCiudad(u.der, ciudad);
            Ciudad c = u.ciudad;
            while (c != null) {
                if (c.nombre.equals(ciudad)) {
                    return city = c;
                }
                c = c.sig;
            }
        }
        return null;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Close1;
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
