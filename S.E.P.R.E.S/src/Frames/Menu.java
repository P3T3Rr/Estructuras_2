package Frames;

import Metodos.Metodos;
import java.awt.HeadlessException;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Menu extends javax.swing.JFrame {

    Login login;
    Metodos met;
    boolean modi = false;

    public Menu(Login login, Metodos met) {
        this.login = login;
        this.met = met;
        initComponents();
        Name.setText(login.usuario.nombre);
        this.setTitle("Register");
        this.setResizable(false);
        this.setSize(1386, 759);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/ico.png")).getImage());

        modificar.setVisible(modi);
        //add(modificar);
        id.setText(login.usuario.cedula + "");
        Nombre.setText(login.usuario.nombre);
        Contraseña.setText(login.usuario.contraseña);
        Telefono.setText(login.usuario.telefono + "");

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        modificar = new javax.swing.JPanel();
        Reportes1 = new javax.swing.JButton();
        Cancel = new javax.swing.JButton();
        ok = new javax.swing.JButton();
        id = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        Nombre = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        Contraseña = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        Telefono = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        Close = new javax.swing.JButton();
        Close1 = new javax.swing.JButton();
        ModificarUsuario = new javax.swing.JButton();
        EliminarUsuario = new javax.swing.JButton();
        Other = new javax.swing.JButton();
        Load = new javax.swing.JButton();
        NewGame = new javax.swing.JButton();
        Name = new javax.swing.JLabel();
        Reportes = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(null);

        modificar.setLayout(null);

        Reportes1.setBackground(new java.awt.Color(153, 7, 0));
        Reportes1.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 48)); // NOI18N
        Reportes1.setForeground(new java.awt.Color(255, 255, 255));
        Reportes1.setText("MODIFY USER DATA");
        Reportes1.setBorder(null);
        Reportes1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Reportes1ActionPerformed(evt);
            }
        });
        modificar.add(Reportes1);
        Reportes1.setBounds(30, 30, 440, 70);

        Cancel.setBackground(new java.awt.Color(153, 7, 0));
        Cancel.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 48)); // NOI18N
        Cancel.setForeground(new java.awt.Color(255, 255, 255));
        Cancel.setText("CANCEL");
        Cancel.setBorder(null);
        Cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CancelActionPerformed(evt);
            }
        });
        modificar.add(Cancel);
        Cancel.setBounds(30, 420, 200, 70);

        ok.setBackground(new java.awt.Color(153, 7, 0));
        ok.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 48)); // NOI18N
        ok.setForeground(new java.awt.Color(255, 255, 255));
        ok.setText("MODIFY");
        ok.setBorder(null);
        ok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okActionPerformed(evt);
            }
        });
        modificar.add(ok);
        ok.setBounds(270, 420, 200, 70);

        id.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 45)); // NOI18N
        id.setForeground(new java.awt.Color(255, 255, 255));
        id.setText("000");
        modificar.add(id);
        id.setBounds(260, 140, 160, 40);

        jLabel3.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 45)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Name");
        modificar.add(jLabel3);
        jLabel3.setBounds(330, 220, 70, 40);

        Nombre.setBackground(new java.awt.Color(51, 51, 51));
        Nombre.setFont(new java.awt.Font("Consolas", 0, 24)); // NOI18N
        Nombre.setForeground(new java.awt.Color(255, 255, 255));
        Nombre.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        modificar.add(Nombre);
        Nombre.setBounds(100, 220, 161, 40);

        jLabel4.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 45)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Password");
        modificar.add(jLabel4);
        jLabel4.setBounds(290, 280, 110, 40);

        Contraseña.setBackground(new java.awt.Color(51, 51, 51));
        Contraseña.setFont(new java.awt.Font("Consolas", 0, 24)); // NOI18N
        Contraseña.setForeground(new java.awt.Color(255, 255, 255));
        Contraseña.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        modificar.add(Contraseña);
        Contraseña.setBounds(100, 280, 161, 40);

        jLabel5.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 45)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Phone");
        modificar.add(jLabel5);
        jLabel5.setBounds(320, 340, 80, 40);

        Telefono.setBackground(new java.awt.Color(51, 51, 51));
        Telefono.setFont(new java.awt.Font("Consolas", 0, 24)); // NOI18N
        Telefono.setForeground(new java.awt.Color(255, 255, 255));
        Telefono.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        modificar.add(Telefono);
        Telefono.setBounds(100, 340, 161, 40);

        jLabel6.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 45)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Your ID is");
        modificar.add(jLabel6);
        jLabel6.setBounds(130, 140, 120, 40);

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Menu1.jpg"))); // NOI18N
        modificar.add(jLabel2);
        jLabel2.setBounds(0, 0, 500, 520);

        getContentPane().add(modificar);
        modificar.setBounds(890, 120, 500, 520);

        Close.setForeground(new java.awt.Color(0, 0, 0));
        Close.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/close_opt1.png"))); // NOI18N
        Close.setToolTipText("Close");
        Close.setBorder(null);
        Close.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CloseActionPerformed(evt);
            }
        });
        getContentPane().add(Close);
        Close.setBounds(1300, 20, 70, 70);

        Close1.setForeground(new java.awt.Color(0, 0, 0));
        Close1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/close_opt.png"))); // NOI18N
        Close1.setToolTipText("Sign off");
        Close1.setBorder(null);
        Close1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Close1ActionPerformed(evt);
            }
        });
        getContentPane().add(Close1);
        Close1.setBounds(1060, 20, 70, 70);

        ModificarUsuario.setBackground(new java.awt.Color(0, 0, 0));
        ModificarUsuario.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 48)); // NOI18N
        ModificarUsuario.setForeground(new java.awt.Color(255, 0, 51));
        ModificarUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Editar.png"))); // NOI18N
        ModificarUsuario.setToolTipText("Modify User");
        ModificarUsuario.setAlignmentX(0.5F);
        ModificarUsuario.setBorder(null);
        ModificarUsuario.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ModificarUsuario.setIconTextGap(10);
        ModificarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ModificarUsuarioActionPerformed(evt);
            }
        });
        getContentPane().add(ModificarUsuario);
        ModificarUsuario.setBounds(1140, 20, 70, 70);

        EliminarUsuario.setBackground(new java.awt.Color(0, 0, 0));
        EliminarUsuario.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 48)); // NOI18N
        EliminarUsuario.setForeground(new java.awt.Color(255, 0, 51));
        EliminarUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/borrar.png"))); // NOI18N
        EliminarUsuario.setToolTipText("Delete User");
        EliminarUsuario.setAlignmentX(0.5F);
        EliminarUsuario.setBorder(null);
        EliminarUsuario.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        EliminarUsuario.setIconTextGap(10);
        EliminarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EliminarUsuarioActionPerformed(evt);
            }
        });
        getContentPane().add(EliminarUsuario);
        EliminarUsuario.setBounds(1220, 20, 70, 70);

        Other.setBackground(new java.awt.Color(153, 204, 255));
        Other.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/OtherCity.png"))); // NOI18N
        Other.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OtherActionPerformed(evt);
            }
        });
        getContentPane().add(Other);
        Other.setBounds(940, 270, 370, 150);

        Load.setBackground(new java.awt.Color(153, 204, 255));
        Load.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/LoadCity.png"))); // NOI18N
        Load.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LoadActionPerformed(evt);
            }
        });
        getContentPane().add(Load);
        Load.setBounds(80, 270, 370, 150);

        NewGame.setBackground(new java.awt.Color(153, 204, 255));
        NewGame.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/NewGame.png"))); // NOI18N
        NewGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NewGameActionPerformed(evt);
            }
        });
        getContentPane().add(NewGame);
        NewGame.setBounds(510, 270, 370, 150);

        Name.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 60)); // NOI18N
        Name.setForeground(new java.awt.Color(204, 255, 255));
        Name.setText("NAME USER");
        getContentPane().add(Name);
        Name.setBounds(80, 30, 210, 70);

        Reportes.setBackground(new java.awt.Color(153, 7, 0));
        Reportes.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 48)); // NOI18N
        Reportes.setForeground(new java.awt.Color(255, 255, 255));
        Reportes.setText("REPORT BY TYPE OF INCIDENT (STATISTICS)");
        Reportes.setBorder(null);
        Reportes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ReportesActionPerformed(evt);
            }
        });
        getContentPane().add(Reportes);
        Reportes.setBounds(290, 550, 780, 70);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Menu1.jpg"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 1386, 759);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void CloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CloseActionPerformed
        met.writeUsers();
        this.dispose();
    }//GEN-LAST:event_CloseActionPerformed

    private void Close1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Close1ActionPerformed
        this.dispose();
        Login frame = new Login();
        frame.setVisible(true);
    }//GEN-LAST:event_Close1ActionPerformed

    private void ModificarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ModificarUsuarioActionPerformed
        modi = !modi;
        modificar.setVisible(modi);
    }//GEN-LAST:event_ModificarUsuarioActionPerformed

    private void EliminarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EliminarUsuarioActionPerformed
        // TODO add your handling code here:
        met.eliminarUsuario(login.usuario.cedula);
        JOptionPane.showMessageDialog(null, "User Deleted");

        met.writeUsers();
        Login frame = new Login();
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_EliminarUsuarioActionPerformed

    private void OtherActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OtherActionPerformed
        LoadOtherCity frame = new LoadOtherCity(met, login, this);
        frame.setVisible(true);
    }//GEN-LAST:event_OtherActionPerformed

    private void NewGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NewGameActionPerformed
        NuevoJuego frame = new NuevoJuego(login, met);
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_NewGameActionPerformed

    private void ReportesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ReportesActionPerformed

    }//GEN-LAST:event_ReportesActionPerformed

    private void Reportes1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Reportes1ActionPerformed

    }//GEN-LAST:event_Reportes1ActionPerformed

    private void CancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CancelActionPerformed
        modi = !modi;
        modificar.setVisible(modi);
    }//GEN-LAST:event_CancelActionPerformed

    private void okActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okActionPerformed
        try {
            if (Nombre.getText().isEmpty() || Contraseña.getText().isEmpty() || Telefono.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Incomplete space...!!");
            } else {
                int cedula = Integer.parseInt(id.getText());
                String nombre = Nombre.getText();
                String contraseña = Contraseña.getText();
                int telefono = Integer.parseInt(Telefono.getText());
                met.modificarUsuario(met.raiz, cedula, nombre, contraseña, telefono);
                JOptionPane.showMessageDialog(null, "Modified Data...!!");
                met.writeUsers();

                modi = !modi;
                this.dispose();
                Menu frame = new Menu(login, met);
                frame.setVisible(true);
            }
        } catch (HeadlessException | NumberFormatException i) {
            JOptionPane.showMessageDialog(null, "The ID must be numeric...!!");
        }
    }//GEN-LAST:event_okActionPerformed

    private void LoadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LoadActionPerformed
        Load frame = new Load(login, met, this);
        frame.setVisible(true);
    }//GEN-LAST:event_LoadActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Cancel;
    private javax.swing.JButton Close;
    private javax.swing.JButton Close1;
    private javax.swing.JTextField Contraseña;
    private javax.swing.JButton EliminarUsuario;
    private javax.swing.JButton Load;
    private javax.swing.JButton ModificarUsuario;
    private javax.swing.JLabel Name;
    private javax.swing.JButton NewGame;
    private javax.swing.JTextField Nombre;
    private javax.swing.JButton Other;
    private javax.swing.JButton Reportes;
    private javax.swing.JButton Reportes1;
    private javax.swing.JTextField Telefono;
    private javax.swing.JLabel id;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel modificar;
    private javax.swing.JButton ok;
    // End of variables declaration//GEN-END:variables
}
