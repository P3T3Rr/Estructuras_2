package Frames;

import Clases.Bomberos;
import Clases.Casa;
import Clases.Ciudad;
import Clases.CruzRoja;
import Clases.Hospital;
import Clases.Mall;
import Clases.Policia;
import Metodos.Metodos;
import java.awt.HeadlessException;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class NuevoJuego extends javax.swing.JFrame {

    Login login;
    Metodos met;

    Ciudad ciudad = new Ciudad();

    boolean casas = false;
    boolean policias = false;
    boolean bomberos = false;
    boolean mall = false;
    boolean cruzRoja = false;
    boolean hospital = false;

    public NuevoJuego(Login login, Metodos met) {
        initComponents();
        this.setTitle("New Game");
        this.setResizable(false);
        this.setSize(1500, 870);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/ico.png")).getImage());
        this.login = login;
        this.met = met;

        CrearCiudad.setVisible(true);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        CrearCiudad = new javax.swing.JPanel();
        jLabel46 = new javax.swing.JLabel();
        Nombre = new javax.swing.JTextField();
        AgregarCasa = new javax.swing.JButton();
        AgregarMall = new javax.swing.JButton();
        AgregarPolicia = new javax.swing.JButton();
        AgregarBomberos = new javax.swing.JButton();
        AgregarCruzRoja = new javax.swing.JButton();
        AgregarHospital = new javax.swing.JButton();
        jLabel45 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        GuardarNuevaCiudad = new javax.swing.JButton();
        Limpiar = new javax.swing.JButton();
        FondoCrearCiudad = new javax.swing.JLabel();
        Close = new javax.swing.JButton();
        TituloCC = new javax.swing.JLabel();
        Guardar = new javax.swing.JButton();
        Atras = new javax.swing.JButton();
        Edificio1 = new javax.swing.JButton();
        Edificio2 = new javax.swing.JButton();
        Edificio3 = new javax.swing.JButton();
        Edificio4 = new javax.swing.JButton();
        Edificio5 = new javax.swing.JButton();
        Edificio6 = new javax.swing.JButton();
        Edificio7 = new javax.swing.JButton();
        Edificio8 = new javax.swing.JButton();
        Edificio9 = new javax.swing.JButton();
        Edificio10 = new javax.swing.JButton();
        Edificio11 = new javax.swing.JButton();
        Edificio12 = new javax.swing.JButton();
        Edificio13 = new javax.swing.JButton();
        Edificio14 = new javax.swing.JButton();
        Edificio15 = new javax.swing.JButton();
        Edificio16 = new javax.swing.JButton();
        Edificio17 = new javax.swing.JButton();
        Edificio18 = new javax.swing.JButton();
        Edificio19 = new javax.swing.JButton();
        Edificio20 = new javax.swing.JButton();
        Edificio21 = new javax.swing.JButton();
        Edificio22 = new javax.swing.JButton();
        Edificio23 = new javax.swing.JButton();
        Edificio24 = new javax.swing.JButton();
        Edificio25 = new javax.swing.JButton();
        Edificio26 = new javax.swing.JButton();
        Edificio27 = new javax.swing.JButton();
        Edificio28 = new javax.swing.JButton();
        Edificio29 = new javax.swing.JButton();
        Edificio30 = new javax.swing.JButton();
        Edificio31 = new javax.swing.JButton();
        Edificio32 = new javax.swing.JButton();
        Edificio33 = new javax.swing.JButton();
        Edificio34 = new javax.swing.JButton();
        Edificio35 = new javax.swing.JButton();
        Edificio36 = new javax.swing.JButton();
        Ciudad = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        jLabel75 = new javax.swing.JLabel();
        jLabel64 = new javax.swing.JLabel();
        jLabel74 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(null);

        CrearCiudad.setLayout(null);

        jLabel46.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 45)); // NOI18N
        jLabel46.setForeground(new java.awt.Color(255, 255, 255));
        jLabel46.setText("City's Name");
        CrearCiudad.add(jLabel46);
        jLabel46.setBounds(100, 40, 145, 40);

        Nombre.setBackground(new java.awt.Color(51, 51, 51));
        Nombre.setFont(new java.awt.Font("Consolas", 0, 24)); // NOI18N
        Nombre.setForeground(new java.awt.Color(255, 255, 255));
        Nombre.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        CrearCiudad.add(Nombre);
        Nombre.setBounds(280, 40, 160, 40);

        AgregarCasa.setBackground(new java.awt.Color(51, 51, 51));
        AgregarCasa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Botones Medianos/casa_opt.png"))); // NOI18N
        AgregarCasa.setBorder(null);
        AgregarCasa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgregarCasaActionPerformed(evt);
            }
        });
        CrearCiudad.add(AgregarCasa);
        AgregarCasa.setBounds(30, 110, 130, 130);

        AgregarMall.setBackground(new java.awt.Color(51, 51, 51));
        AgregarMall.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Botones Medianos/Mall_opt.png"))); // NOI18N
        AgregarMall.setBorder(null);
        AgregarMall.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgregarMallActionPerformed(evt);
            }
        });
        CrearCiudad.add(AgregarMall);
        AgregarMall.setBounds(220, 110, 130, 130);

        AgregarPolicia.setBackground(new java.awt.Color(51, 51, 51));
        AgregarPolicia.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Botones Medianos/EstacionDePolicia_opt.png"))); // NOI18N
        AgregarPolicia.setBorder(null);
        AgregarPolicia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgregarPoliciaActionPerformed(evt);
            }
        });
        CrearCiudad.add(AgregarPolicia);
        AgregarPolicia.setBounds(30, 310, 130, 130);

        AgregarBomberos.setBackground(new java.awt.Color(51, 51, 51));
        AgregarBomberos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Botones Medianos/EstacionDeBomberos_opt.png"))); // NOI18N
        AgregarBomberos.setBorder(null);
        AgregarBomberos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgregarBomberosActionPerformed(evt);
            }
        });
        CrearCiudad.add(AgregarBomberos);
        AgregarBomberos.setBounds(410, 310, 130, 130);

        AgregarCruzRoja.setBackground(new java.awt.Color(51, 51, 51));
        AgregarCruzRoja.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Botones Medianos/cruzRoja_opt.png"))); // NOI18N
        AgregarCruzRoja.setBorder(null);
        AgregarCruzRoja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgregarCruzRojaActionPerformed(evt);
            }
        });
        CrearCiudad.add(AgregarCruzRoja);
        AgregarCruzRoja.setBounds(220, 310, 130, 130);

        AgregarHospital.setBackground(new java.awt.Color(51, 51, 51));
        AgregarHospital.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Botones Medianos/hospital_opt.png"))); // NOI18N
        AgregarHospital.setBorder(null);
        AgregarHospital.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgregarHospitalActionPerformed(evt);
            }
        });
        CrearCiudad.add(AgregarHospital);
        AgregarHospital.setBounds(410, 110, 130, 130);

        jLabel45.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 30)); // NOI18N
        jLabel45.setForeground(new java.awt.Color(255, 255, 255));
        jLabel45.setText("Add Shopping Center");
        CrearCiudad.add(jLabel45);
        jLabel45.setBounds(220, 240, 140, 40);

        jLabel47.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 30)); // NOI18N
        jLabel47.setForeground(new java.awt.Color(255, 255, 255));
        jLabel47.setText("Add Fire Station");
        CrearCiudad.add(jLabel47);
        jLabel47.setBounds(420, 440, 107, 40);

        jLabel48.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 30)); // NOI18N
        jLabel48.setForeground(new java.awt.Color(255, 255, 255));
        jLabel48.setText("Add Police Station");
        CrearCiudad.add(jLabel48);
        jLabel48.setBounds(30, 440, 130, 40);

        jLabel49.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 30)); // NOI18N
        jLabel49.setForeground(new java.awt.Color(255, 255, 255));
        jLabel49.setText("Add Red Cross");
        CrearCiudad.add(jLabel49);
        jLabel49.setBounds(240, 440, 100, 40);

        jLabel50.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 30)); // NOI18N
        jLabel50.setForeground(new java.awt.Color(255, 255, 255));
        jLabel50.setText("Add Hospital");
        CrearCiudad.add(jLabel50);
        jLabel50.setBounds(430, 240, 90, 40);

        jLabel51.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 30)); // NOI18N
        jLabel51.setForeground(new java.awt.Color(255, 255, 255));
        jLabel51.setText("Add House");
        CrearCiudad.add(jLabel51);
        jLabel51.setBounds(60, 240, 70, 40);

        GuardarNuevaCiudad.setBackground(new java.awt.Color(153, 7, 0));
        GuardarNuevaCiudad.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 48)); // NOI18N
        GuardarNuevaCiudad.setForeground(new java.awt.Color(255, 255, 255));
        GuardarNuevaCiudad.setText("SAVE");
        GuardarNuevaCiudad.setBorder(null);
        GuardarNuevaCiudad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GuardarNuevaCiudadActionPerformed(evt);
            }
        });
        CrearCiudad.add(GuardarNuevaCiudad);
        GuardarNuevaCiudad.setBounds(310, 510, 160, 70);

        Limpiar.setBackground(new java.awt.Color(153, 7, 0));
        Limpiar.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 48)); // NOI18N
        Limpiar.setForeground(new java.awt.Color(255, 255, 255));
        Limpiar.setText("RESTART");
        Limpiar.setBorder(null);
        Limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LimpiarActionPerformed(evt);
            }
        });
        CrearCiudad.add(Limpiar);
        Limpiar.setBounds(100, 510, 160, 70);

        FondoCrearCiudad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/FondoJuego.jpg"))); // NOI18N
        CrearCiudad.add(FondoCrearCiudad);
        FondoCrearCiudad.setBounds(0, -70, 580, 790);

        getContentPane().add(CrearCiudad);
        CrearCiudad.setBounds(930, 200, 580, 730);

        Close.setForeground(new java.awt.Color(0, 0, 0));
        Close.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/close_opt1.png"))); // NOI18N
        Close.setToolTipText("Close");
        Close.setBorder(null);
        Close.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CloseActionPerformed(evt);
            }
        });
        getContentPane().add(Close);
        Close.setBounds(1410, 30, 70, 70);

        TituloCC.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 50)); // NOI18N
        TituloCC.setForeground(new java.awt.Color(204, 255, 255));
        TituloCC.setText("CREATE CITY");
        getContentPane().add(TituloCC);
        TituloCC.setBounds(1150, 150, 150, 60);

        Guardar.setBackground(new java.awt.Color(0, 0, 0));
        Guardar.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 48)); // NOI18N
        Guardar.setForeground(new java.awt.Color(255, 0, 51));
        Guardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/guardar_opt.png"))); // NOI18N
        Guardar.setToolTipText("Save City");
        Guardar.setAlignmentX(0.5F);
        Guardar.setBorder(null);
        Guardar.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        Guardar.setIconTextGap(10);
        Guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GuardarActionPerformed(evt);
            }
        });
        getContentPane().add(Guardar);
        Guardar.setBounds(1330, 30, 70, 70);

        Atras.setForeground(new java.awt.Color(0, 0, 0));
        Atras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/close_opt.png"))); // NOI18N
        Atras.setToolTipText("Main");
        Atras.setBorder(null);
        Atras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AtrasActionPerformed(evt);
            }
        });
        getContentPane().add(Atras);
        Atras.setBounds(1250, 30, 70, 70);

        Edificio1.setBackground(new java.awt.Color(51, 51, 51));
        Edificio1.setBorder(null);
        Edificio1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio1ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio1);
        Edificio1.setBounds(120, 90, 80, 80);

        Edificio2.setBackground(new java.awt.Color(51, 51, 51));
        Edificio2.setBorder(null);
        Edificio2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio2ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio2);
        Edificio2.setBounds(210, 90, 80, 80);

        Edificio3.setBackground(new java.awt.Color(51, 51, 51));
        Edificio3.setBorder(null);
        Edificio3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio3ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio3);
        Edificio3.setBounds(120, 180, 80, 80);

        Edificio4.setBackground(new java.awt.Color(51, 51, 51));
        Edificio4.setBorder(null);
        Edificio4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio4ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio4);
        Edificio4.setBounds(210, 180, 80, 80);

        Edificio5.setBackground(new java.awt.Color(51, 51, 51));
        Edificio5.setBorder(null);
        Edificio5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio5ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio5);
        Edificio5.setBounds(370, 90, 80, 80);

        Edificio6.setBackground(new java.awt.Color(51, 51, 51));
        Edificio6.setBorder(null);
        Edificio6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio6ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio6);
        Edificio6.setBounds(460, 90, 80, 80);

        Edificio7.setBackground(new java.awt.Color(51, 51, 51));
        Edificio7.setBorder(null);
        Edificio7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio7ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio7);
        Edificio7.setBounds(370, 180, 80, 80);

        Edificio8.setBackground(new java.awt.Color(51, 51, 51));
        Edificio8.setBorder(null);
        Edificio8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio8ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio8);
        Edificio8.setBounds(460, 180, 80, 80);

        Edificio9.setBackground(new java.awt.Color(51, 51, 51));
        Edificio9.setBorder(null);
        Edificio9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio9ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio9);
        Edificio9.setBounds(620, 90, 80, 80);

        Edificio10.setBackground(new java.awt.Color(51, 51, 51));
        Edificio10.setBorder(null);
        Edificio10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio10ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio10);
        Edificio10.setBounds(710, 90, 80, 80);

        Edificio11.setBackground(new java.awt.Color(51, 51, 51));
        Edificio11.setBorder(null);
        Edificio11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio11ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio11);
        Edificio11.setBounds(620, 180, 80, 80);

        Edificio12.setBackground(new java.awt.Color(51, 51, 51));
        Edificio12.setBorder(null);
        Edificio12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio12ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio12);
        Edificio12.setBounds(710, 180, 80, 80);

        Edificio13.setBackground(new java.awt.Color(51, 51, 51));
        Edificio13.setBorder(null);
        Edificio13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio13ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio13);
        Edificio13.setBounds(120, 340, 80, 80);

        Edificio14.setBackground(new java.awt.Color(51, 51, 51));
        Edificio14.setBorder(null);
        Edificio14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio14ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio14);
        Edificio14.setBounds(210, 340, 80, 80);

        Edificio15.setBackground(new java.awt.Color(51, 51, 51));
        Edificio15.setBorder(null);
        Edificio15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio15ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio15);
        Edificio15.setBounds(120, 430, 80, 80);

        Edificio16.setBackground(new java.awt.Color(51, 51, 51));
        Edificio16.setBorder(null);
        Edificio16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio16ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio16);
        Edificio16.setBounds(210, 430, 80, 80);

        Edificio17.setBackground(new java.awt.Color(51, 51, 51));
        Edificio17.setBorder(null);
        Edificio17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio17ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio17);
        Edificio17.setBounds(370, 340, 80, 80);

        Edificio18.setBackground(new java.awt.Color(51, 51, 51));
        Edificio18.setBorder(null);
        Edificio18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio18ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio18);
        Edificio18.setBounds(460, 340, 80, 80);

        Edificio19.setBackground(new java.awt.Color(51, 51, 51));
        Edificio19.setBorder(null);
        Edificio19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio19ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio19);
        Edificio19.setBounds(370, 430, 80, 80);

        Edificio20.setBackground(new java.awt.Color(51, 51, 51));
        Edificio20.setBorder(null);
        Edificio20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio20ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio20);
        Edificio20.setBounds(460, 430, 80, 80);

        Edificio21.setBackground(new java.awt.Color(51, 51, 51));
        Edificio21.setBorder(null);
        Edificio21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio21ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio21);
        Edificio21.setBounds(620, 340, 80, 80);

        Edificio22.setBackground(new java.awt.Color(51, 51, 51));
        Edificio22.setBorder(null);
        Edificio22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio22ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio22);
        Edificio22.setBounds(710, 340, 80, 80);

        Edificio23.setBackground(new java.awt.Color(51, 51, 51));
        Edificio23.setBorder(null);
        Edificio23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio23ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio23);
        Edificio23.setBounds(620, 430, 80, 80);

        Edificio24.setBackground(new java.awt.Color(51, 51, 51));
        Edificio24.setBorder(null);
        Edificio24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio24ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio24);
        Edificio24.setBounds(710, 430, 80, 80);

        Edificio25.setBackground(new java.awt.Color(51, 51, 51));
        Edificio25.setBorder(null);
        Edificio25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio25ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio25);
        Edificio25.setBounds(120, 590, 80, 80);

        Edificio26.setBackground(new java.awt.Color(51, 51, 51));
        Edificio26.setBorder(null);
        Edificio26.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio26ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio26);
        Edificio26.setBounds(210, 590, 80, 80);

        Edificio27.setBackground(new java.awt.Color(51, 51, 51));
        Edificio27.setBorder(null);
        Edificio27.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio27ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio27);
        Edificio27.setBounds(120, 680, 80, 80);

        Edificio28.setBackground(new java.awt.Color(51, 51, 51));
        Edificio28.setBorder(null);
        Edificio28.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio28ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio28);
        Edificio28.setBounds(210, 680, 80, 80);

        Edificio29.setBackground(new java.awt.Color(51, 51, 51));
        Edificio29.setBorder(null);
        Edificio29.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio29ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio29);
        Edificio29.setBounds(370, 590, 80, 80);

        Edificio30.setBackground(new java.awt.Color(51, 51, 51));
        Edificio30.setBorder(null);
        Edificio30.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio30ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio30);
        Edificio30.setBounds(460, 590, 80, 80);

        Edificio31.setBackground(new java.awt.Color(51, 51, 51));
        Edificio31.setBorder(null);
        Edificio31.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio31ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio31);
        Edificio31.setBounds(370, 680, 80, 80);

        Edificio32.setBackground(new java.awt.Color(51, 51, 51));
        Edificio32.setBorder(null);
        Edificio32.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio32ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio32);
        Edificio32.setBounds(460, 680, 80, 80);

        Edificio33.setBackground(new java.awt.Color(51, 51, 51));
        Edificio33.setBorder(null);
        Edificio33.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio33ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio33);
        Edificio33.setBounds(620, 590, 80, 80);

        Edificio34.setBackground(new java.awt.Color(51, 51, 51));
        Edificio34.setBorder(null);
        Edificio34.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio34ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio34);
        Edificio34.setBounds(710, 590, 80, 80);

        Edificio35.setBackground(new java.awt.Color(51, 51, 51));
        Edificio35.setBorder(null);
        Edificio35.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio35ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio35);
        Edificio35.setBounds(620, 680, 80, 80);

        Edificio36.setBackground(new java.awt.Color(51, 51, 51));
        Edificio36.setBorder(null);
        Edificio36.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Edificio36ActionPerformed(evt);
            }
        });
        getContentPane().add(Edificio36);
        Edificio36.setBounds(710, 680, 80, 80);

        Ciudad.setBackground(new java.awt.Color(153, 7, 0));
        Ciudad.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 1, 48)); // NOI18N
        Ciudad.setForeground(new java.awt.Color(255, 255, 255));
        Ciudad.setText("CREATE CITY");
        Ciudad.setBorder(null);
        Ciudad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CiudadActionPerformed(evt);
            }
        });
        getContentPane().add(Ciudad);
        Ciudad.setBounds(970, 30, 200, 70);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle1.png"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(100, 30, 210, 70);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle6.png"))); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(60, 70, 70, 210);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle1.png"))); // NOI18N
        getContentPane().add(jLabel3);
        jLabel3.setBounds(350, 30, 210, 70);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle4.png"))); // NOI18N
        getContentPane().add(jLabel4);
        jLabel4.setBounds(280, 70, 100, 210);

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle1.png"))); // NOI18N
        getContentPane().add(jLabel5);
        jLabel5.setBounds(600, 30, 210, 70);

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle5.png"))); // NOI18N
        getContentPane().add(jLabel6);
        jLabel6.setBounds(780, 70, 70, 210);

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle4.png"))); // NOI18N
        getContentPane().add(jLabel7);
        jLabel7.setBounds(530, 70, 100, 210);

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle3.png"))); // NOI18N
        getContentPane().add(jLabel8);
        jLabel8.setBounds(350, 250, 210, 100);

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle3.png"))); // NOI18N
        getContentPane().add(jLabel9);
        jLabel9.setBounds(100, 250, 210, 100);

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle3.png"))); // NOI18N
        getContentPane().add(jLabel10);
        jLabel10.setBounds(600, 250, 210, 100);

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle6.png"))); // NOI18N
        getContentPane().add(jLabel12);
        jLabel12.setBounds(60, 320, 70, 210);

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle4.png"))); // NOI18N
        getContentPane().add(jLabel14);
        jLabel14.setBounds(280, 320, 100, 210);

        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle5.png"))); // NOI18N
        getContentPane().add(jLabel16);
        jLabel16.setBounds(780, 320, 70, 210);

        jLabel17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle4.png"))); // NOI18N
        getContentPane().add(jLabel17);
        jLabel17.setBounds(530, 320, 100, 210);

        jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle3.png"))); // NOI18N
        getContentPane().add(jLabel18);
        jLabel18.setBounds(350, 500, 210, 100);

        jLabel19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle3.png"))); // NOI18N
        getContentPane().add(jLabel19);
        jLabel19.setBounds(100, 500, 210, 100);

        jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle3.png"))); // NOI18N
        getContentPane().add(jLabel20);
        jLabel20.setBounds(600, 500, 210, 100);

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle6.png"))); // NOI18N
        getContentPane().add(jLabel13);
        jLabel13.setBounds(60, 570, 70, 210);

        jLabel21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle4.png"))); // NOI18N
        getContentPane().add(jLabel21);
        jLabel21.setBounds(280, 570, 100, 210);

        jLabel23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle5.png"))); // NOI18N
        getContentPane().add(jLabel23);
        jLabel23.setBounds(780, 570, 70, 210);

        jLabel24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle4.png"))); // NOI18N
        getContentPane().add(jLabel24);
        jLabel24.setBounds(530, 570, 100, 210);

        jLabel25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle2.png"))); // NOI18N
        getContentPane().add(jLabel25);
        jLabel25.setBounds(350, 750, 210, 70);

        jLabel26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle2.png"))); // NOI18N
        getContentPane().add(jLabel26);
        jLabel26.setBounds(100, 750, 210, 70);

        jLabel27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Calles/calle2.png"))); // NOI18N
        getContentPane().add(jLabel27);
        jLabel27.setBounds(600, 750, 210, 70);

        jLabel11.setBackground(new java.awt.Color(153, 153, 153));
        jLabel11.setForeground(new java.awt.Color(153, 153, 153));
        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel11);
        jLabel11.setBounds(810, 30, 40, 40);

        jLabel15.setBackground(new java.awt.Color(153, 153, 153));
        jLabel15.setForeground(new java.awt.Color(153, 153, 153));
        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel15);
        jLabel15.setBounds(60, 30, 40, 40);

        jLabel22.setBackground(new java.awt.Color(153, 153, 153));
        jLabel22.setForeground(new java.awt.Color(153, 153, 153));
        jLabel22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel22);
        jLabel22.setBounds(310, 30, 40, 40);

        jLabel28.setBackground(new java.awt.Color(153, 153, 153));
        jLabel28.setForeground(new java.awt.Color(153, 153, 153));
        jLabel28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel28);
        jLabel28.setBounds(560, 30, 40, 40);

        jLabel29.setBackground(new java.awt.Color(153, 153, 153));
        jLabel29.setForeground(new java.awt.Color(153, 153, 153));
        jLabel29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel29);
        jLabel29.setBounds(810, 30, 40, 40);

        jLabel30.setBackground(new java.awt.Color(153, 153, 153));
        jLabel30.setForeground(new java.awt.Color(153, 153, 153));
        jLabel30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel30);
        jLabel30.setBounds(560, 30, 40, 40);

        jLabel31.setBackground(new java.awt.Color(153, 153, 153));
        jLabel31.setForeground(new java.awt.Color(153, 153, 153));
        jLabel31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel31);
        jLabel31.setBounds(310, 30, 40, 40);

        jLabel32.setBackground(new java.awt.Color(153, 153, 153));
        jLabel32.setForeground(new java.awt.Color(153, 153, 153));
        jLabel32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel32);
        jLabel32.setBounds(60, 30, 40, 40);

        jLabel33.setBackground(new java.awt.Color(153, 153, 153));
        jLabel33.setForeground(new java.awt.Color(153, 153, 153));
        jLabel33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel33);
        jLabel33.setBounds(810, 280, 40, 40);

        jLabel34.setBackground(new java.awt.Color(153, 153, 153));
        jLabel34.setForeground(new java.awt.Color(153, 153, 153));
        jLabel34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel34);
        jLabel34.setBounds(560, 280, 40, 40);

        jLabel35.setBackground(new java.awt.Color(153, 153, 153));
        jLabel35.setForeground(new java.awt.Color(153, 153, 153));
        jLabel35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel35);
        jLabel35.setBounds(310, 280, 40, 40);

        jLabel36.setBackground(new java.awt.Color(153, 153, 153));
        jLabel36.setForeground(new java.awt.Color(153, 153, 153));
        jLabel36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel36);
        jLabel36.setBounds(60, 280, 40, 40);

        jLabel37.setBackground(new java.awt.Color(153, 153, 153));
        jLabel37.setForeground(new java.awt.Color(153, 153, 153));
        jLabel37.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel37);
        jLabel37.setBounds(810, 530, 40, 40);

        jLabel38.setBackground(new java.awt.Color(153, 153, 153));
        jLabel38.setForeground(new java.awt.Color(153, 153, 153));
        jLabel38.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel38);
        jLabel38.setBounds(560, 530, 40, 40);

        jLabel39.setBackground(new java.awt.Color(153, 153, 153));
        jLabel39.setForeground(new java.awt.Color(153, 153, 153));
        jLabel39.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel39);
        jLabel39.setBounds(310, 530, 40, 40);

        jLabel40.setBackground(new java.awt.Color(153, 153, 153));
        jLabel40.setForeground(new java.awt.Color(153, 153, 153));
        jLabel40.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel40);
        jLabel40.setBounds(60, 530, 40, 40);

        jLabel41.setBackground(new java.awt.Color(153, 153, 153));
        jLabel41.setForeground(new java.awt.Color(153, 153, 153));
        jLabel41.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel41);
        jLabel41.setBounds(810, 780, 40, 40);

        jLabel42.setBackground(new java.awt.Color(153, 153, 153));
        jLabel42.setForeground(new java.awt.Color(153, 153, 153));
        jLabel42.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel42);
        jLabel42.setBounds(560, 780, 40, 40);

        jLabel43.setBackground(new java.awt.Color(153, 153, 153));
        jLabel43.setForeground(new java.awt.Color(153, 153, 153));
        jLabel43.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel43);
        jLabel43.setBounds(310, 780, 40, 40);

        jLabel44.setBackground(new java.awt.Color(153, 153, 153));
        jLabel44.setForeground(new java.awt.Color(153, 153, 153));
        jLabel44.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruce.jpg"))); // NOI18N
        getContentPane().add(jLabel44);
        jLabel44.setBounds(60, 780, 40, 40);

        jLabel55.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/FondoJuego.jpg"))); // NOI18N
        getContentPane().add(jLabel55);
        jLabel55.setBounds(930, 0, 580, 720);

        jLabel75.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cesped.jpg"))); // NOI18N
        getContentPane().add(jLabel75);
        jLabel75.setBounds(0, 440, 500, 490);

        jLabel64.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cesped.jpg"))); // NOI18N
        getContentPane().add(jLabel64);
        jLabel64.setBounds(0, 0, 500, 490);

        jLabel74.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cesped.jpg"))); // NOI18N
        getContentPane().add(jLabel74);
        jLabel74.setBounds(430, 430, 500, 490);

        jLabel59.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cesped.jpg"))); // NOI18N
        getContentPane().add(jLabel59);
        jLabel59.setBounds(430, -50, 500, 490);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void CloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CloseActionPerformed
        this.dispose();
    }//GEN-LAST:event_CloseActionPerformed

    private void CiudadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CiudadActionPerformed
        CrearCiudad.setVisible(true);
        Nombre.setText("");
        ciudad = new Ciudad();
        BorrarCiudad();
    }//GEN-LAST:event_CiudadActionPerformed

    private void GuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GuardarActionPerformed
        met.writeUsers();
    }//GEN-LAST:event_GuardarActionPerformed

    private void AtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AtrasActionPerformed
        Menu frame = new Menu(login, met);
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_AtrasActionPerformed

    private void LimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LimpiarActionPerformed
        BorrarCiudad();
    }//GEN-LAST:event_LimpiarActionPerformed

    private void AgregarCasaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgregarCasaActionPerformed
        casas = true;
        policias = false;
        bomberos = false;
        mall = false;
        cruzRoja = false;
        hospital = false;
    }//GEN-LAST:event_AgregarCasaActionPerformed

    private void AgregarMallActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgregarMallActionPerformed
        casas = false;
        policias = false;
        bomberos = false;
        mall = true;
        cruzRoja = false;
        hospital = false;
    }//GEN-LAST:event_AgregarMallActionPerformed

    private void AgregarBomberosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgregarBomberosActionPerformed
        casas = false;
        policias = false;
        bomberos = true;
        mall = false;
        cruzRoja = false;
        hospital = false;
    }//GEN-LAST:event_AgregarBomberosActionPerformed

    private void AgregarPoliciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgregarPoliciaActionPerformed
        casas = false;
        policias = true;
        bomberos = false;
        mall = false;
        cruzRoja = false;
        hospital = false;
    }//GEN-LAST:event_AgregarPoliciaActionPerformed

    private void AgregarCruzRojaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgregarCruzRojaActionPerformed
        casas = false;
        policias = false;
        bomberos = false;
        mall = false;
        cruzRoja = true;
        hospital = false;
    }//GEN-LAST:event_AgregarCruzRojaActionPerformed

    private void AgregarHospitalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgregarHospitalActionPerformed
        casas = false;
        policias = false;
        bomberos = false;
        mall = false;
        cruzRoja = false;
        hospital = true;
    }//GEN-LAST:event_AgregarHospitalActionPerformed

    private void Edificio1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio1ActionPerformed
        if (casas) {
            Edificio1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E1.tipo = edificio;
        } else if (hospital) {
            Edificio1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E1.tipo = edificio;
        } else if (mall) {
            Edificio1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E1.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E1.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E1.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E1.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio1ActionPerformed

    private void GuardarNuevaCiudadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GuardarNuevaCiudadActionPerformed
        ciudad.setNombre(Nombre.getText());
        met.CrearCiudad(login.usuario, ciudad);
        CrearCiudad.setVisible(false);
        
        met.writeUsers();
        
        Simulacion frame = new Simulacion(login, met, ciudad);
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_GuardarNuevaCiudadActionPerformed

    private void Edificio3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio3ActionPerformed
        if (casas) {
            Edificio3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E19.tipo = edificio;
        } else if (hospital) {
            Edificio3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E19.tipo = edificio;
        } else if (mall) {
            Edificio3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E19.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E19.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E19.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E19.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio3ActionPerformed

    private void Edificio2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio2ActionPerformed
        if (casas) {
            Edificio2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E22.tipo = edificio;
        } else if (hospital) {
            Edificio2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E22.tipo = edificio;
        } else if (mall) {
            Edificio2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E22.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E22.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E22.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E22.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio2ActionPerformed

    private void Edificio5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio5ActionPerformed
        if (casas) {
            Edificio5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E2.tipo = edificio;
        } else if (hospital) {
            Edificio5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E2.tipo = edificio;
        } else if (mall) {
            Edificio5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E2.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E2.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E2.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E2.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio5ActionPerformed

    private void Edificio6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio6ActionPerformed
        if (casas) {
            Edificio6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E28.tipo = edificio;
        } else if (hospital) {
            Edificio6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E28.tipo = edificio;
        } else if (mall) {
            Edificio6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E28.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E28.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E28.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E28.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio6ActionPerformed

    private void Edificio9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio9ActionPerformed
        if (casas) {
            Edificio9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E3.tipo = edificio;
        } else if (hospital) {
            Edificio9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E3.tipo = edificio;
        } else if (mall) {
            Edificio9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E3.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E3.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E3.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E3.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio9ActionPerformed

    private void Edificio10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio10ActionPerformed
        if (casas) {
            Edificio10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E34.tipo = edificio;
        } else if (hospital) {
            Edificio10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E34.tipo = edificio;
        } else if (mall) {
            Edificio10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E34.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E34.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E34.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E34.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio10ActionPerformed

    private void Edificio4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio4ActionPerformed
        if (casas) {
            Edificio4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E5.tipo = edificio;
        } else if (hospital) {
            Edificio4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E5.tipo = edificio;
        } else if (mall) {
            Edificio4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E5.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E5.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E5.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E5.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio4ActionPerformed

    private void Edificio7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio7ActionPerformed
        if (casas) {
            Edificio7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E23.tipo = edificio;
        } else if (hospital) {
            Edificio7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E23.tipo = edificio;
        } else if (mall) {
            Edificio7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E23.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E23.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E23.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E23.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio7ActionPerformed

    private void Edificio8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio8ActionPerformed
        if (casas) {
            Edificio8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E7.tipo = edificio;
        } else if (hospital) {
            Edificio8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E7.tipo = edificio;
        } else if (mall) {
            Edificio8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E7.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E7.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E7.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E7.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio8ActionPerformed

    private void Edificio11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio11ActionPerformed
        if (casas) {
            Edificio11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E29.tipo = edificio;
        } else if (hospital) {
            Edificio11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E29.tipo = edificio;
        } else if (mall) {
            Edificio11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E29.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E29.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E29.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E29.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio11ActionPerformed

    private void Edificio12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio12ActionPerformed
        if (casas) {
            Edificio12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E9.tipo = edificio;
        } else if (hospital) {
            Edificio12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E9.tipo = edificio;
        } else if (mall) {
            Edificio12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E9.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E9.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E9.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E9.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio12ActionPerformed

    private void Edificio13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio13ActionPerformed
        if (casas) {
            Edificio13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E4.tipo = edificio;
        } else if (hospital) {
            Edificio13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E4.tipo = edificio;
        } else if (mall) {
            Edificio13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E4.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E4.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E4.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E4.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio13ActionPerformed

    private void Edificio14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio14ActionPerformed
        if (casas) {
            Edificio14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E24.tipo = edificio;
        } else if (hospital) {
            Edificio14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E24.tipo = edificio;
        } else if (mall) {
            Edificio14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E24.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E24.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E24.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E24.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio14ActionPerformed

    private void Edificio17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio17ActionPerformed
        if (casas) {
            Edificio17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E6.tipo = edificio;
        } else if (hospital) {
            Edificio17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E6.tipo = edificio;
        } else if (mall) {
            Edificio17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E6.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E6.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E6.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E6.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio17ActionPerformed

    private void Edificio18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio18ActionPerformed
        if (casas) {
            Edificio18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E30.tipo = edificio;
        } else if (hospital) {
            Edificio18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E30.tipo = edificio;
        } else if (mall) {
            Edificio18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E30.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E30.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E30.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E30.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio18ActionPerformed

    private void Edificio21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio21ActionPerformed
        if (casas) {
            Edificio21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E8.tipo = edificio;
        } else if (hospital) {
            Edificio21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E8.tipo = edificio;
        } else if (mall) {
            Edificio21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E8.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E8.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E8.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E8.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio21ActionPerformed

    private void Edificio22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio22ActionPerformed
        if (casas) {
            Edificio22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E35.tipo = edificio;
        } else if (hospital) {
            Edificio22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E35.tipo = edificio;
        } else if (mall) {
            Edificio22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E35.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E35.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E35.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E35.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio22ActionPerformed

    private void Edificio15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio15ActionPerformed
        if (casas) {
            Edificio15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E20.tipo = edificio;
        } else if (hospital) {
            Edificio15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E20.tipo = edificio;
        } else if (mall) {
            Edificio15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E20.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E20.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E20.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E20.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio15ActionPerformed

    private void Edificio16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio16ActionPerformed
        if (casas) {
            Edificio16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E11.tipo = edificio;
        } else if (hospital) {
            Edificio16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E11.tipo = edificio;
        } else if (mall) {
            Edificio16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E11.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E11.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E11.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E11.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio16ActionPerformed

    private void Edificio19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio19ActionPerformed
        if (casas) {
            Edificio19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E25.tipo = edificio;
        } else if (hospital) {
            Edificio19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E25.tipo = edificio;
        } else if (mall) {
            Edificio19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E25.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E25.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E25.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E25.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio19ActionPerformed

    private void Edificio20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio20ActionPerformed
        if (casas) {
            Edificio20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E13.tipo = edificio;
        } else if (hospital) {
            Edificio20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E13.tipo = edificio;
        } else if (mall) {
            Edificio20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E13.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E13.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E13.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E13.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio20ActionPerformed

    private void Edificio23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio23ActionPerformed
        if (casas) {
            Edificio23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E31.tipo = edificio;
        } else if (hospital) {
            Edificio23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E31.tipo = edificio;
        } else if (mall) {
            Edificio23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E31.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E31.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E31.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E31.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio23ActionPerformed

    private void Edificio24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio24ActionPerformed
        if (casas) {
            Edificio24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E15.tipo = edificio;
        } else if (hospital) {
            Edificio24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E15.tipo = edificio;
        } else if (mall) {
            Edificio24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E15.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E15.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E15.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E15.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio24ActionPerformed

    private void Edificio25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio25ActionPerformed
        if (casas) {
            Edificio25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E10.tipo = edificio;
        } else if (hospital) {
            Edificio25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E10.tipo = edificio;
        } else if (mall) {
            Edificio25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E10.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E10.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E10.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E10.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio25ActionPerformed

    private void Edificio26ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio26ActionPerformed
        if (casas) {
            Edificio26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E26.tipo = edificio;
        } else if (hospital) {
            Edificio26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E26.tipo = edificio;
        } else if (mall) {
            Edificio26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E26.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E26.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E26.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E26.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio26ActionPerformed

    private void Edificio29ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio29ActionPerformed
        if (casas) {
            Edificio29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E12.tipo = edificio;
        } else if (hospital) {
            Edificio29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E12.tipo = edificio;
        } else if (mall) {
            Edificio29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E12.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E12.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E12.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E12.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio29ActionPerformed

    private void Edificio30ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio30ActionPerformed
        if (casas) {
            Edificio30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E32.tipo = edificio;
        } else if (hospital) {
            Edificio30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E32.tipo = edificio;
        } else if (mall) {
            Edificio30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E32.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E32.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E32.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E32.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio30ActionPerformed

    private void Edificio33ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio33ActionPerformed
        if (casas) {
            Edificio33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E14.tipo = edificio;
        } else if (hospital) {
            Edificio33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E14.tipo = edificio;
        } else if (mall) {
            Edificio33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E14.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E14.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E14.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E14.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio33ActionPerformed

    private void Edificio34ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio34ActionPerformed
        if (casas) {
            Edificio34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E36.tipo = edificio;
        } else if (hospital) {
            Edificio34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E36.tipo = edificio;
        } else if (mall) {
            Edificio34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E36.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E36.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E36.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E36.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio34ActionPerformed

    private void Edificio27ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio27ActionPerformed
        if (casas) {
            Edificio27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E21.tipo = edificio;
        } else if (hospital) {
            Edificio27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E21.tipo = edificio;
        } else if (mall) {
            Edificio27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E21.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E21.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E21.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E21.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio27ActionPerformed

    private void Edificio28ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio28ActionPerformed
        if (casas) {
            Edificio28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E16.tipo = edificio;
        } else if (hospital) {
            Edificio28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E16.tipo = edificio;
        } else if (mall) {
            Edificio28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E16.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E16.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E16.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E16.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio28ActionPerformed

    private void Edificio31ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio31ActionPerformed
        if (casas) {
            Edificio31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E27.tipo = edificio;
        } else if (hospital) {
            Edificio31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E27.tipo = edificio;
        } else if (mall) {
            Edificio31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E27.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E27.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E27.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E27.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio31ActionPerformed

    private void Edificio32ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio32ActionPerformed
        if (casas) {
            Edificio32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E17.tipo = edificio;
        } else if (hospital) {
            Edificio32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E17.tipo = edificio;
        } else if (mall) {
            Edificio32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E17.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E17.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E17.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E17.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio32ActionPerformed

    private void Edificio35ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio35ActionPerformed
        if (casas) {
            Edificio35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E33.tipo = edificio;
        } else if (hospital) {
            Edificio35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E33.tipo = edificio;
        } else if (mall) {
            Edificio35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E33.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E33.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E33.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E33.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio35ActionPerformed

    private void Edificio36ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Edificio36ActionPerformed
        if (casas) {
            Edificio36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/casa_opt.png")));
            Casa edificio = new Casa();
            ciudad.E18.tipo = edificio;
        } else if (hospital) {
            Edificio36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/hospital_opt.png")));
            Hospital edificio = new Hospital();
            ciudad.E18.tipo = edificio;
        } else if (mall) {
            Edificio36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Mall_opt.png")));
            Mall edificio = new Mall();
            ciudad.E18.tipo = edificio;
        } else if (policias) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDePolicia_opt.png")));
                Policia edificio = new Policia();
                edificio.setearUnidades(vehiculos);
                ciudad.E18.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (bomberos) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/EstacionDeBomberos_opt.png")));
                Bomberos edificio = new Bomberos();
                edificio.setearUnidades(vehiculos);
                ciudad.E18.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        } else if (cruzRoja) {
            try {
                int vehiculos = Integer.parseInt(JOptionPane.showInputDialog("Enter the total of vehicles of this station...!!"));
                Edificio36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cruzRoja.png")));
                CruzRoja edificio = new CruzRoja();
                edificio.setearUnidades(vehiculos);
                ciudad.E18.tipo = edificio;
            } catch (HeadlessException | NumberFormatException i) {
                JOptionPane.showMessageDialog(null, "The building can't be inserted\n" + " because the entered simbol\n" + " can't be recognized as a number...!!");
            }
        }
    }//GEN-LAST:event_Edificio36ActionPerformed

    public void BorrarCiudad() {
        Edificio2.setIcon(null);
        Edificio1.setIcon(null);
        Edificio3.setIcon(null);
        Edificio4.setIcon(null);
        Edificio5.setIcon(null);
        Edificio6.setIcon(null);
        Edificio7.setIcon(null);
        Edificio8.setIcon(null);
        Edificio9.setIcon(null);
        Edificio10.setIcon(null);
        Edificio11.setIcon(null);
        Edificio12.setIcon(null);
        Edificio13.setIcon(null);
        Edificio14.setIcon(null);
        Edificio15.setIcon(null);
        Edificio16.setIcon(null);
        Edificio17.setIcon(null);
        Edificio18.setIcon(null);
        Edificio19.setIcon(null);
        Edificio20.setIcon(null);
        Edificio21.setIcon(null);
        Edificio22.setIcon(null);
        Edificio23.setIcon(null);
        Edificio24.setIcon(null);
        Edificio25.setIcon(null);
        Edificio26.setIcon(null);
        Edificio27.setIcon(null);
        Edificio28.setIcon(null);
        Edificio29.setIcon(null);
        Edificio30.setIcon(null);
        Edificio31.setIcon(null);
        Edificio32.setIcon(null);
        Edificio33.setIcon(null);
        Edificio34.setIcon(null);
        Edificio35.setIcon(null);
        Edificio36.setIcon(null);

        ciudad.E1.tipo = null;
        ciudad.E2.tipo = null;
        ciudad.E3.tipo = null;
        ciudad.E4.tipo = null;
        ciudad.E5.tipo = null;
        ciudad.E6.tipo = null;
        ciudad.E7.tipo = null;
        ciudad.E8.tipo = null;
        ciudad.E9.tipo = null;
        ciudad.E10.tipo = null;
        ciudad.E11.tipo = null;
        ciudad.E12.tipo = null;
        ciudad.E13.tipo = null;
        ciudad.E14.tipo = null;
        ciudad.E15.tipo = null;
        ciudad.E16.tipo = null;
        ciudad.E17.tipo = null;
        ciudad.E18.tipo = null;
        ciudad.E19.tipo = null;
        ciudad.E20.tipo = null;
        ciudad.E21.tipo = null;
        ciudad.E22.tipo = null;
        ciudad.E23.tipo = null;
        ciudad.E24.tipo = null;
        ciudad.E25.tipo = null;
        ciudad.E26.tipo = null;
        ciudad.E27.tipo = null;
        ciudad.E28.tipo = null;
        ciudad.E29.tipo = null;
        ciudad.E30.tipo = null;
        ciudad.E31.tipo = null;
        ciudad.E32.tipo = null;
        ciudad.E33.tipo = null;
        ciudad.E34.tipo = null;
        ciudad.E35.tipo = null;
        ciudad.E36.tipo = null;
    }
    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AgregarBomberos;
    private javax.swing.JButton AgregarCasa;
    private javax.swing.JButton AgregarCruzRoja;
    private javax.swing.JButton AgregarHospital;
    private javax.swing.JButton AgregarMall;
    private javax.swing.JButton AgregarPolicia;
    private javax.swing.JButton Atras;
    private javax.swing.JButton Ciudad;
    private javax.swing.JButton Close;
    private javax.swing.JPanel CrearCiudad;
    private javax.swing.JButton Edificio1;
    private javax.swing.JButton Edificio10;
    private javax.swing.JButton Edificio11;
    private javax.swing.JButton Edificio12;
    private javax.swing.JButton Edificio13;
    private javax.swing.JButton Edificio14;
    private javax.swing.JButton Edificio15;
    private javax.swing.JButton Edificio16;
    private javax.swing.JButton Edificio17;
    private javax.swing.JButton Edificio18;
    private javax.swing.JButton Edificio19;
    private javax.swing.JButton Edificio2;
    private javax.swing.JButton Edificio20;
    private javax.swing.JButton Edificio21;
    private javax.swing.JButton Edificio22;
    private javax.swing.JButton Edificio23;
    private javax.swing.JButton Edificio24;
    private javax.swing.JButton Edificio25;
    private javax.swing.JButton Edificio26;
    private javax.swing.JButton Edificio27;
    private javax.swing.JButton Edificio28;
    private javax.swing.JButton Edificio29;
    private javax.swing.JButton Edificio3;
    private javax.swing.JButton Edificio30;
    private javax.swing.JButton Edificio31;
    private javax.swing.JButton Edificio32;
    private javax.swing.JButton Edificio33;
    private javax.swing.JButton Edificio34;
    private javax.swing.JButton Edificio35;
    private javax.swing.JButton Edificio36;
    private javax.swing.JButton Edificio4;
    private javax.swing.JButton Edificio5;
    private javax.swing.JButton Edificio6;
    private javax.swing.JButton Edificio7;
    private javax.swing.JButton Edificio8;
    private javax.swing.JButton Edificio9;
    private javax.swing.JLabel FondoCrearCiudad;
    private javax.swing.JButton Guardar;
    private javax.swing.JButton GuardarNuevaCiudad;
    private javax.swing.JButton Limpiar;
    private javax.swing.JTextField Nombre;
    private javax.swing.JLabel TituloCC;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    // End of variables declaration//GEN-END:variables
}
