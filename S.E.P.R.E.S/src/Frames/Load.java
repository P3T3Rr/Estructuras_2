package Frames;

import Clases.Ciudad;
import Clases.Usuario;
import Metodos.Metodos;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Load extends javax.swing.JFrame {

    Login login;
    Metodos met;
    Menu menu;

    public Load(Login login, Metodos met,Menu menu) {
        initComponents();
        this.setTitle("Load Game");
        this.setResizable(false);
        this.setSize(660, 430);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.login = login;
        this.met = met;
        this.menu = menu;

        rellenarComboBox();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox2 = new javax.swing.JComboBox<>();
        Close1 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        setPreferredSize(new java.awt.Dimension(660, 430));
        getContentPane().setLayout(null);

        jComboBox2.setBackground(new java.awt.Color(153, 7, 0));
        jComboBox2.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jComboBox2.setForeground(new java.awt.Color(0, 0, 0));
        getContentPane().add(jComboBox2);
        jComboBox2.setBounds(100, 210, 450, 50);

        Close1.setForeground(new java.awt.Color(0, 0, 0));
        Close1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/close_opt1.png"))); // NOI18N
        Close1.setToolTipText("Sign off");
        Close1.setBorder(null);
        Close1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Close1ActionPerformed(evt);
            }
        });
        getContentPane().add(Close1);
        Close1.setBounds(560, 10, 70, 70);

        jButton1.setBackground(new java.awt.Color(153, 7, 0));
        jButton1.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 48)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("MODIFY CITY");
        jButton1.setBorder(null);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1);
        jButton1.setBounds(340, 120, 210, 70);

        jButton2.setBackground(new java.awt.Color(153, 7, 0));
        jButton2.setFont(new java.awt.Font("Gill Sans MT Ext Condensed Bold", 0, 48)); // NOI18N
        jButton2.setForeground(new java.awt.Color(255, 255, 255));
        jButton2.setText("LOAD CITY");
        jButton2.setBorder(null);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2);
        jButton2.setBounds(100, 120, 210, 70);

        jLabel1.setBackground(new java.awt.Color(153, 7, 0));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Menu1.jpg"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(-360, -130, 1230, 690);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Close1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Close1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_Close1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if (login.usuario.ciudad != null) {
            String city = jComboBox2.getSelectedItem().toString();
            Ciudad ciudad = met.buscarCiudad(login.usuario, city);
            Simulacion frame = new Simulacion(login, met, ciudad);
            frame.setVisible(true);
            this.dispose();
            menu.dispose();
        } else {
            JOptionPane.showMessageDialog(this, "There isn't any city...!!");
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (login.usuario.ciudad != null) {
            String city = jComboBox2.getSelectedItem().toString();
            Ciudad ciudad = met.buscarCiudad(login.usuario, city);
            ModificarCiudad frame = new ModificarCiudad(login, met, ciudad,menu);
            frame.setVisible(true);
            this.dispose();
            menu.dispose();
        }
        else {
            JOptionPane.showMessageDialog(this, "There isn't any city...!!");
        }
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Close1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables

    public void rellenarComboBox() {
        Usuario u = login.usuario;
        Ciudad x = u.ciudad;
        while (x != null) {
            jComboBox2.addItem(x.nombre);
            x = x.sig;
        }
    }
}
