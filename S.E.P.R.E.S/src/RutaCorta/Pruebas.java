/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RutaCorta;

import java.util.ArrayList;

/**
 *
 * @author kevin
 */
public class Pruebas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        metodos met = new metodos();
        met.insertVertice("A");
        met.insertVertice("B");
        met.insertVertice("C");
        met.insertVertice("D");
        met.insertVertice("E");
        met.insertVertice("F");
        met.insertVertice("G");
        met.insertVertice("H");
        met.insertVertice("I");
        met.insertArco(met.buscarV("A"), met.buscarV("B"), 6);
        met.insertArco(met.buscarV("A"), met.buscarV("C"), 10);
        met.insertArco(met.buscarV("B"), met.buscarV("C"), 7);
        met.insertArco(met.buscarV("B"), met.buscarV("E"), 1);
        met.insertArco(met.buscarV("B"), met.buscarV("D"), 3);
        met.insertArco(met.buscarV("C"), met.buscarV("D"), 11);
        met.insertArco(met.buscarV("C"), met.buscarV("F"), 9);
        met.insertArco(met.buscarV("C"), met.buscarV("B"), 5);
        met.insertArco(met.buscarV("E"), met.buscarV("D"), 3);
        met.insertArco(met.buscarV("E"), met.buscarV("G"), 2);
        met.insertArco(met.buscarV("D"), met.buscarV("F"), 5);
        met.insertArco(met.buscarV("D"), met.buscarV("G"), 4);
        met.insertArco(met.buscarV("D"), met.buscarV("H"), 6);
        met.insertArco(met.buscarV("D"), met.buscarV("E"), 3);
        met.insertArco(met.buscarV("F"), met.buscarV("H"), 13);
        met.insertArco(met.buscarV("F"), met.buscarV("D"), 5);
        met.insertArco(met.buscarV("G"), met.buscarV("I"), 6);
        met.insertArco(met.buscarV("G"), met.buscarV("H"), 5);
        met.insertArco(met.buscarV("H"), met.buscarV("I"), 7);
        met.insertArco(met.buscarV("H"), met.buscarV("G"), 3);

        ArrayList<vertice> m = new ArrayList();
        m.add(met.buscarV("B"));
        m.add(met.buscarV("C"));

        met.multipametroRuta(m, met.buscarV("I"));
        System.out.println("[" + met.rutaS + "]" + "==" + met.menor);
        System.out.println(met.rutas);
        System.out.println();
        met.limpiar_marcas();

        met.Ruta(met.buscarV("A"), met.buscarV("I"), "", 0);
        met.listarRuta();
        System.out.println("[" + met.ruta + "]" + "==" + met.Dmenor);
        System.out.println(met.rutas);
        System.out.println();
        met.limpiar_marcas();

        met.Ruta(met.buscarV("B"), met.buscarV("I"), "", 0);
        System.out.println("[" + met.ruta + "]" + "==" + met.Dmenor);
        System.out.println();
        met.limpiar_marcas();

        met.Ruta(met.buscarV("C"), met.buscarV("I"), "", 0);
        System.out.println("[" + met.ruta + "]" + "==" + met.Dmenor);
        System.out.println();
        met.limpiar_marcas();

        met.Ruta(met.buscarV("B"), met.buscarV("F"), "", 0);
        System.out.println("[" + met.ruta + "]" + "==" + met.Dmenor);
        System.out.println();
        met.limpiar_marcas();
    }
}
