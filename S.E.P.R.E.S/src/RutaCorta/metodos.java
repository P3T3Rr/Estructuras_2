/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RutaCorta;

import java.util.ArrayList;

/**
 *
 * @author kevin
 */
public class metodos {

    public arco inicioA;
    public vertice Grafo;
    int Dmenor = 10000000;//Guarda la distancia menor, es alto para que inicie con la primera encontrada y interfiera 
    int menor = 0;
    String ruta = "";// guarda la ruta menor en un string enviado por el metodo
    String rutaS = "";
    ArrayList<vertice> rutas;

    public String insertVertice(String n) {
        vertice nuevo = new vertice(n, false);
        if (Grafo == null) {
            Grafo = nuevo;
            return "insertado";
        }
        if (buscarV(n) != null) {
            return "repetido";
        }
        nuevo.sigV = Grafo;
        Grafo = nuevo;
        return "insertado";
    }

    public String insertArco(vertice origen, vertice destino, int peso) {
        arco nuevo = new arco(peso);
        if (buscarA(origen, destino)) {
            nuevo.destino = destino;
            if (origen.sigA == null) {
                origen.sigA = nuevo;
            } else {
                nuevo.sigA = origen.sigA;
                origen.sigA = nuevo;
            }
            return "insertado";
        }
        return "repetido";
    }

    public vertice buscarV(String name) {
        vertice aux = Grafo;
        while (aux != null) {
            if (aux.nombre.equals(name)) {
                return aux;
            }
            aux = aux.sigV;
        }
        return null;
    }

    public boolean buscarA(vertice origen, vertice destino) {
        if (origen.sigA == null) {
            return true;
        }
        arco aux = origen.sigA;
        while (aux != null) {
            if (aux.destino == destino) {
                return false;
            }
            aux = aux.sigA;
        }
        return true;
    }

    public boolean eliminarArco(vertice origen, vertice destino) {
        if (origen.sigA == null) {
            return false;
        }
        if (origen.sigA.destino == destino) {
            origen.sigA = origen.sigA.sigA;
            return true;
        }
        arco aux = origen.sigA.sigA;
        arco anterior = origen.sigA;
        while (aux.destino != null) {
            if (aux.destino == destino) {
                anterior.sigA = aux.sigA;
                return true;
            }
            anterior = aux;
            aux = aux.sigA;
        }
        return false;
    }

    public String eliminarV(vertice v) {
        vertice aux = Grafo;
        while (aux != null) {
            if (aux.sigA != null) {
                arco temp = aux.sigA;
                while (temp != null) {
                    eliminarArco(aux, v);
                    temp = temp.sigA;
                }
            }
            aux = aux.sigV;
        }
        if (Grafo == v) {
            Grafo = Grafo.sigV;
            return "Borrado";
        }
        aux = Grafo;
        vertice anterior = Grafo;
        while (aux.sigV != null) {
            if (aux.sigV == v) {
                aux.sigV = aux.sigV.sigV;
                return "Borrado";
            }
            aux = aux.sigV;
        }
        return "No hay vertice";
    }

    public void Ruta(vertice origen, vertice destino, String trayecto, int distancia) {//metodo de busqueda de ruta 
        if (distancia > Dmenor) {
            return;
        }//Evita rutas mas largas que la ultima correcta encontrada
        if ((origen == null) | (origen.marca == true)) {//si ya lo visitó o es nulo retorna en la recursividad
            return;
        }

        if (origen.nombre.equals(destino.nombre))//cuando logra llegar compara con la ruta menor encontrada,si es menor la guarda y retorna 
        {

            if (distancia < Dmenor) {
                Dmenor = distancia;
                trayecto = trayecto + origen.nombre;
                ruta = trayecto;
            }
            return;
        }
        origen.marca = true;//marca como visitado
        arco a = origen.sigA;
        while (a != null) {//recorre todos los arcos del nodo donde se encuentra para buscar el camino correcto
            Ruta(a.destino, destino, trayecto + origen.nombre, distancia + a.peso);
            a = a.sigA;
        }
        origen.marca = false;//cuando sale de los arcos de ese vertice lo desmarca para buscar en otro arco del vertice origen anterior
    }

    public void listarRuta() {
        rutas = new ArrayList();
        for (int i = 0; i < ruta.length(); i++) {
            vertice aux = buscarV(String.valueOf(ruta.charAt(i)));
            rutas.add(aux);
        }
    }

    public void multipametroRuta(ArrayList<vertice> inicios, vertice destino) {
        menor = Dmenor;
        rutaS = ruta;
        for (vertice inicio : inicios) {
            limpiar_marcas();
            Ruta(inicio, destino, "", 0);
            if (Dmenor < menor) {
                rutaS = ruta;
                menor = Dmenor;
                listarRuta();
            }
        }
    }

    public void limpiar_marcas() {
        Dmenor = 10000000;
        ruta = "";
        vertice temp = Grafo;
        if (Grafo == null) {
            return;
        }
        while (temp != null) {
            temp.marca = false;
            temp = temp.sigV;
        }
    }
}
