package Metodos;

import Clases.Ciudad;
import Clases.Incidente;
import Clases.Usuario;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Metodos implements Serializable {

    public Usuario raiz;
    public Incidente raizInsidente;
    String mensaje;
    Usuario encontrado;
    boolean insertado;

    //Metodo para insertar usuarios en un arbol binario
    public boolean insertarUsuario(Usuario aux, String nombre, int cedula, int telefono, String contraseña) {
        Usuario nuevo = new Usuario(nombre, cedula, contraseña, telefono);
        if (raiz == null) {
            raiz = nuevo;
            return insertado = true;
        }
        if (cedula < aux.cedula) {
            if (aux.izq == null) {
                aux.izq = nuevo;
                return insertado = true;
            } else {
                insertarUsuario(aux.izq, nombre, cedula, telefono, contraseña);
            }
        } else if (cedula > aux.cedula) {
            if (aux.der == null) {
                aux.der = nuevo;
                return insertado = true;
            } else {
                insertarUsuario(aux.der, nombre, cedula, telefono, contraseña);
            }
        } else {
            return false;
        }

        return insertado;
    }

    //Metodo para buscar un usuario
    public Usuario buscarUsuario(Usuario aux, int id) {
        if (aux == null) {
            return encontrado = null;
        }

        if (aux.cedula == id) {
            return encontrado = aux;
        }

        if (id < aux.cedula) {
            if (aux.izq != null) {
                if (aux.izq.cedula == id) {
                    return encontrado = aux.izq;
                } else {
                    buscarUsuario(aux.izq, id);
                }
            } else {
                return encontrado = null;
            }
        } else if (id > aux.cedula) {
            if (aux.der != null) {
                if (aux.der.cedula == id) {
                    return encontrado = aux.der;
                } else {
                    buscarUsuario(aux.der, id);
                }
            } else {
                return encontrado = null;
            }
        } else {
            return null;
        }
        return encontrado;
    }

    //Metodo para modificar usuarios
    public String modificarUsuario(Usuario raiz, int idBuscado, String nuevoNombre, String NuevaContrasena, int nuevoTelefono) {
        Usuario buscado = buscarUsuario(raiz, idBuscado);
        if (buscado != null) {
            buscado.setNombre(nuevoNombre);
            buscado.setContraseña(NuevaContrasena);
            buscado.setTelefono(nuevoTelefono);
            return "Successfully Modified...!!";
        }
        return "This user does not exist...!!";
    }

    //Metoto para eliminar un usuario
    public boolean eliminarUsuario(int d) {
        Usuario auxiliar = raiz;
        Usuario padre = raiz;
        boolean esHijoIzq = true;
        while (auxiliar.cedula != d) {
            padre = auxiliar;
            if (d < auxiliar.cedula) {
                esHijoIzq = true;
                auxiliar = auxiliar.izq;
            } else {
                esHijoIzq = false;
                auxiliar = auxiliar.der;
            }
            if (auxiliar == null) {
                return false; // no lo encontro
            }
        }//fin del while
        if (auxiliar.izq == null && auxiliar.der == null) {
            if (auxiliar == raiz) {
                raiz = null;
            } else if (esHijoIzq) {
                padre.izq = null;
            } else {
                padre.der = null;
            }
        } else if (auxiliar.der == null) {
            if (auxiliar == raiz) {
                raiz = auxiliar.izq;
            } else if (esHijoIzq) {
                padre.izq = auxiliar.izq;
            } else {
                padre.der = auxiliar.izq;
            }
        } else if (auxiliar.izq == null) {
            if (auxiliar == raiz) {
                raiz = auxiliar.der;
            } else if (esHijoIzq) {
                padre.izq = auxiliar.der;
            } else {
                padre.der = auxiliar.izq;
            }
        } else {
            Usuario reemplazo = obtenerUsuarioReemplazo(auxiliar);
            if (auxiliar == raiz) {
                raiz = reemplazo;
            } else if (esHijoIzq) {
                padre.izq = reemplazo;
            } else {
                padre.der = reemplazo;
            }
            reemplazo.izq = auxiliar.izq;
        }
        return true;
    }

    //Metodo encargado de devolver el nodo reemplazo
    public Usuario obtenerUsuarioReemplazo(Usuario nodoReemp) {
        Usuario reemplazarPadre = nodoReemp;
        Usuario reemplazo = nodoReemp;
        Usuario auxiliar = nodoReemp.der;
        while (auxiliar != null) {
            reemplazarPadre = reemplazo;
            reemplazo = auxiliar;
            auxiliar = auxiliar.izq;
        }
        if (reemplazo != nodoReemp.der) {
            reemplazarPadre.izq = reemplazo.der;
            reemplazo.der = nodoReemp.der;
        }
        return reemplazo;
    }

    //Method for reading characters created from a file
    public void readUsers() {
        File userData = new File("src/Datas/Users.ser");
        try {
            ObjectInputStream archivo = new ObjectInputStream(new FileInputStream(userData));
            Usuario us = (Usuario) archivo.readObject();
            raiz = us;
            imprimirPosOrden(raiz);
        } catch (IOException | ClassNotFoundException exc) {
            System.out.println("El usuario no se a leido" + exc);
        }
    }

    public void writeUsers() {
        File userData = new File("src/Datas/Users.ser");
        if (userData.exists()) {
            userData.delete();
        }

        try {
            ObjectOutputStream archivo = new ObjectOutputStream(new FileOutputStream(userData));
            archivo.writeObject(raiz);
        } catch (IOException i) {
            System.out.println("El usuario no se guardo...!!" + i);
        }
    }

    public void imprimirPosOrden(Usuario U) {
        if (U != null) {
            imprimirPosOrden(U.izq);
            imprimirPosOrden(U.der);
            System.out.println("Usuario: " + U.nombre + " -> " + U.cedula);
        }
    }

    public void CrearCiudad(Usuario usuario, Ciudad ciudad) {
        if (usuario.ciudad == null) {
            usuario.ciudad = ciudad;
        } else {
            ciudad.sig = usuario.ciudad;
            usuario.ciudad = ciudad;
        }
    }

    public Ciudad buscarCiudad(Usuario usuario, String nombre) {
        Ciudad temp = usuario.ciudad;
        if (temp == null) {
            return null;
        }
        while (temp != null) {
            if (temp.nombre.equals(nombre)) {
                return temp;
            }
            temp = temp.sig;
        }
        return null;
    }

    public void insertarIncidente(Ciudad raizCiudad, Incidente incidente) {
        if (raizCiudad.raizIncidente == null) {
            raizCiudad.raizIncidente = incidente;
            return;
        }
        Incidente aux = raizCiudad.raizIncidente;
        while (aux.der != null) {
            aux = aux.der;
        }
        aux.der = incidente;
    }
}
