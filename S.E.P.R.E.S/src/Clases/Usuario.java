package Clases;

import java.io.Serializable;

public class Usuario implements Serializable{

    public Usuario izq;
    public Usuario der;
    public String nombre;
    public int cedula;
    public String contraseña;
    public int telefono;
    public Ciudad ciudad;

    public Usuario(String nombre, int cedula, String contraseña, int telefono) {
        this.nombre = nombre;
        this.cedula = cedula;
        this.contraseña = contraseña;
        this.telefono = telefono;
        this.izq = null;
        this.der = null;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
}
