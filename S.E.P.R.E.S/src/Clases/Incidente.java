
package Clases;

import java.io.Serializable;

public class Incidente implements Serializable{
    
    public int Id;
    public String tipo;
    public String nombre;
    public int tiempo;
    public Incidente der, Izq;

    public Incidente(int Id, String tipo, String nombre, int tiempo) {
        this.Id = Id;
        this.tipo = tipo;
        this.nombre = nombre;
        this.tiempo = tiempo;
    }
}
