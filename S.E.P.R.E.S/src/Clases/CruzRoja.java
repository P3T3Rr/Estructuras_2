package Clases;

import java.io.Serializable;
import java.util.ArrayList;

public class CruzRoja extends Edificio implements SetearUnidades, Serializable{

    public ArrayList<VehiculoCruzRoja> ambulancias;

    public CruzRoja() {
        super();
        ambulancias = new ArrayList<>();
    }

    @Override
    public void setearUnidades(int unidades) {
        while (unidades != 0) {
            VehiculoCruzRoja nuevo = new VehiculoCruzRoja();
            ambulancias.add(nuevo);
            unidades--;
        }
    }
}
