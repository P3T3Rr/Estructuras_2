package Clases;

import java.io.Serializable;
import java.util.ArrayList;

public class Policia extends Edificio implements SetearUnidades , Serializable{

    public ArrayList<VehiculoPolicia> patrullas;

    public Policia() {
        super();
        patrullas = new ArrayList<>();
    }

    @Override
    public void setearUnidades(int unidades) {
        while (unidades != 0) {
            VehiculoPolicia nuevo = new VehiculoPolicia();
            patrullas.add(nuevo);
            unidades--;
        }
    }
}
