package Clases;

import java.io.Serializable;

public class Calle implements Serializable{

    public Edificio edificio;
    public int distancia;

    public Calle(Edificio edificio, int distancia) {
        this.edificio = edificio;
        this.distancia = distancia;
    }
}
