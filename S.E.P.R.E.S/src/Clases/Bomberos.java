package Clases;

import java.io.Serializable;
import java.util.ArrayList;

public class Bomberos extends Edificio implements SetearUnidades , Serializable{

    public ArrayList<VehiculoBomberos> camionesDeBomberos;

    public Bomberos() {
        super();
        camionesDeBomberos = new ArrayList<>();
    }

    @Override
    public void setearUnidades(int unidades) {
        while (unidades != 0) {
            VehiculoBomberos nuevo = new VehiculoBomberos();
            camionesDeBomberos.add(nuevo);
            unidades--;
        }
    }
}
