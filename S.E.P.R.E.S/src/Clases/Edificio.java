package Clases;

import java.io.Serializable;

public class Edificio implements Serializable{

    public Calle calleIzquierda, calleDerecha, calleArriba, calleAbajo;
    public int x, y;
    public Edificio tipo;
    public Incidente incidente;
    
    public Edificio() {
        this.calleIzquierda = null;
        this.calleDerecha = null;
        this.calleArriba = null;
        this.calleAbajo = null;
        this.tipo = null;
    }

    public void setX_Y(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setTipo(Edificio tipo) {
        this.tipo = tipo;
    }
}
